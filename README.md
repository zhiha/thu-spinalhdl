FFT Project 
============

# 环境配置

参考网址：https://spinalhdl.github.io/SpinalDoc-RTD/master/SpinalHDL/Getting%20Started/getting_started.html

# 项目介绍

本项目实现FFT功能，源码（ffte）实现功能包括任意N点的FFT与IFFT，且拥有并行、串行以及串并组合的模式。

本项目根据源码实现初步面向用户使用的接口（user），目前接口可以实现任意N点FFT的串行模式，不过由于时间原因，接口并未将源码实现的所有功能移植完成，后续会尽快完善。

# 使用说明

user/fft.scala：

串行输入端口：d
串行输出端口：q
数据使能端口：ce，当ce为高电平，输出的q数据为有效数据

test/fft.scala：

针对user/fft.scala进行测试，其中tmp/streamed.log中记录着对应N点测试的结果。

**注：用户将需要进行FFT的数据送入d输入端口，然后根据ce使能信号从q输出端口接收对应FFT后的数据。**

# 贡献者

Prof.Zhao





