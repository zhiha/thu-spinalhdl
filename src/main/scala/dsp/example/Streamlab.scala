package dsp.example

import spinal.core.sim._
import spinal.core._
import spinal.sim._
import scala.util.Random._
import spinal.lib._

class StreamExample extends Component{
    val io = new Bundle{
        val streamInput = slave Stream(UInt(8 bits))
        val flatOutput =  out Vec(UInt(8 bits),2)
        val streamOutput = master Stream(UInt(8 bits))
    }
    val flat = Reg(Vec(UInt(8 bits),2)) 
    val bo = Reg(Bool()) init(true)
    //flat(0) := 0
    //flat(1) := 0
   
    io.flatOutput(0) := flat(0)
    io.flatOutput(1) := flat(1)
    //io.streamInput.ready := bo
    bo := ! bo
    when(io.streamInput.ready && io.streamInput.valid){
        //flat(0) := io.streamInput.payload
        //flat(1) := io.streamInput.payload
    }
    flat(0) := io.streamInput.payload
    flat(1) := io.streamInput.payload
    io.streamOutput <-< io.streamInput
}

object Streamtest extends App{
    SpinalVerilog(new StreamExample())
}

object Streamsim extends App{
    SimConfig.withWave.doSim(new StreamExample()){dut =>
       dut.clockDomain.forkStimulus(10)
       dut.io.streamInput.valid #= true
       for(idx <- 0 to 100){
          dut.clockDomain.waitRisingEdge()
          dut.io.streamInput.valid #= nextBoolean()
          dut.io.streamInput.payload #= nextInt(10)
       }
    }
}