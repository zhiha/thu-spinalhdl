package dsp.example

import spinal.core.sim._
import spinal.core._
import spinal.sim._
import scala.util.Random._

class AreaExample extends Component{
    def isZero(value:UInt) = new Area{
        val comparator = value === 0
    }
    val value = in UInt (8 bits)
    val someLogic = isZero(value)
    val result= out Bool()
    val reg = RegInit(value)
    result := someLogic.comparator

    value.setName("valueData")
    result.setCompositeName(value,"CompareResult")
    reg.setName("regVal")
    someLogic.comparator.setName("Comparator")
}
/**
  * module MyComponent (
        input      [7:0]    value,
        output              result
    );
    wire  someLogic_comparator;

    assign someLogic_comparator = (value == 8'h0);
    assign result = someLogic_comparator;

    endmodule
  */
object Areatest extends App{
    SpinalVerilog(new AreaExample())
}

object Areasim extends App{
    SimConfig.withWave.doSim(new AreaExample()){dut =>
       dut.clockDomain.forkStimulus(10)
       for(idx <- 0 to 100){
          dut.clockDomain.waitRisingEdge()
          dut.value #= nextInt(10)
       }
    }
}