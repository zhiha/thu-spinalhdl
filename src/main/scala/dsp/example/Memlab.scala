package dsp.example

import spinal.core.sim._
import spinal.core._
import spinal.sim._
import scala.util.Random._
import spinal.lib._
import ffte.algorithm.FFTGen
import ffte.core.streamed.{Streamed,streamedComponents}

class MemExample(N:Int) extends Component{
    val io = new Bundle{
        val dataIn = in(Vec(UInt(32 bits),N))
        val dataInValid = in(Bool())
        val dataInReady = out(Bool())
        //val dataOut = out(UInt(32 bits))
        //val dataOutValid = out(Bool())
    }
    val dataIn = Reg(Vec(UInt(32 bits),N))
    val dataInReady = Reg(Bool()) init(true)

    val mem = Mem(UInt(32 bits), wordCount = N)
    val writeAddress = Reg(UInt(log2Up(N) bits)) init(0)
    val writeValid = Reg(Bool()) init(false)

    val readAddress = Reg(UInt(log2Up(N) bits)) init(0)
    val readValid = Reg(Bool()) init(false)
    val readData = Reg(UInt(32 bits)) init(0)

    io.dataInReady := dataInReady

    when(io.dataInValid && dataInReady){
        dataIn := io.dataIn
        dataInReady := False
        writeValid := True
    }

    
    when(writeValid){
        writeAddress := writeAddress + 1
        when(writeAddress === U(N-1)){
            writeAddress := 0
            writeValid := False
            readValid := True
        }
    }

    
    when(readValid){
        readAddress := readAddress + 1
        when(readAddress === U(N-1)){
            readAddress := 0
            readValid := False
            dataInReady := True
        }
    }

    mem.write(
        enable  = writeValid,
        address = writeAddress,
        data    = dataIn(writeAddress)
    )
    
    readData := mem.readSync(
        enable  = readValid,
        address = readAddress
    )

    val inputDelay = 1
    val memWriteDelay = N
    val memReadDelay = N + 1
    def delay = inputDelay + memWriteDelay + memReadDelay

}

class MemStreamExample(N:Int) extends Component{
    val io = new Bundle{
        val dataIn = in(Vec(UInt(32 bits),N))
        val dataInValid = in(Bool())
        val dataInReady = out(Bool())
        val dataOut = out(UInt(32 bits))
        val dataOutReady = in(Bool())
        val dataOutValid = out(Bool())
        val dataOutLast = out(Bool())
    }
    val dataIn = Reg(Vec(UInt(32 bits),N))
    val dataInReady = Reg(Bool()) init(true)

    val mem = Mem(UInt(32 bits), wordCount = N)
    val writeAddress = Reg(UInt(log2Up(N) bits)) init(0)
    val writeValid = Reg(Bool()) init(false)

    val readAddress = Reg(UInt(log2Up(N) bits)) init(0)
    val readValid = Reg(Bool()) init(false)

    val tabel = (for(i <- 0 until N) yield i).toArray
    val tabelIndex = Reg(Vec(UInt(log2Up(N) bits),N))
    for(i <- 0 until N) tabelIndex(i) := tabel(i)

    var data = UInt(32 bits) 
    var dataLast = Reg(Bool()) init(False)
    var dataValid = Reg(Bool()) init(False)
    dataLast := False
    dataValid := False
    

    io.dataInReady := dataInReady

    when(io.dataInValid && dataInReady){
        dataIn := io.dataIn
        dataInReady := False
        writeValid := True
    }

    
    when(writeValid){
        writeAddress := writeAddress + 1
        when(writeAddress === U(N-1)){
            writeAddress := 0
            writeValid := False
            readValid := True
        }
    }

    
    when(readValid && io.dataOutReady){
        readAddress := readAddress + 1
        when(readAddress === U(N-1)){
            readAddress := 0
            readValid := False
            dataInReady := True
            dataLast := True
        }
        dataValid := True
    }

    mem.write(
        enable  = writeValid,
        address = writeAddress,
        data    = dataIn(writeAddress)
    )
    
    data := mem.readSync(
        enable  = readValid && io.dataOutReady,
        address = tabelIndex(readAddress)
    ) 
    io.dataOut := data
    io.dataOutValid := dataValid
    io.dataOutLast := dataLast

    val inputDelay = 1
    val memWriteDelay = N
    val readDelay = 1
    val outputDelay = 1
    def delay = inputDelay + memWriteDelay + readDelay + outputDelay

}
object Memtest extends App{
    SpinalVerilog(new MemExample(3))
}

object Memsim extends App{
    SimConfig.withWave.doSim(new MemExample(3)){dut =>
       dut.clockDomain.forkStimulus(10)
       dut.io.dataInValid #= false
       for(idx <- 0 to 300){
          dut.clockDomain.waitRisingEdge()
          if(dut.io.dataInReady.toBoolean){
            for(i <- 0 until 3){
                dut.io.dataIn(i) #= nextInt(10)
                dut.io.dataInValid #= true
            }
          }

       }
    }
}

object MemStreamsim extends App{
    SimConfig.withWave.doSim(new MemStreamExample(3)){dut =>
       dut.clockDomain.forkStimulus(10)
       dut.io.dataInValid #= false
       dut.io.dataOutReady #= true
       for(idx <- 0 to 100){
           dut.clockDomain.waitRisingEdge()
          if(dut.io.dataOutReady.toBoolean && dut.io.dataOutValid.toBoolean){
              println("打印值")
              println(dut.io.dataOut.toBigInt)
              
          }
          
          if(dut.io.dataInReady.toBoolean){
            println("输入值")
            for(i <- 0 until 3){
                val a = nextInt(10)
                dut.io.dataIn(i) #= a
                println(a)
                dut.io.dataInValid #= true
            }
          }

       }
    }
}