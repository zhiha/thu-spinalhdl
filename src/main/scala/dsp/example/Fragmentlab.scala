package dsp.example

import spinal.core.sim._
import spinal.core._
import spinal.sim._
import scala.util.Random._
import spinal.lib._

class FragmentExample extends Component{
    val input = in(Stream(Fragment(Vec(UInt(8 bits),1))))
    val output = out(Stream(Fragment(Vec(UInt(8 bits),1))))
    val regVal = Reg(Vec(UInt(8 bits),1))

    output := input
    regVal := input.payload.fragment
}

object Fragmenttest extends App{
    SpinalVerilog(new FragmentExample())
}

object Fragmentsim extends App{
    SimConfig.withWave.doSim(new FragmentExample()){dut =>
       dut.clockDomain.forkStimulus(10)
       dut.input.valid #= true
       dut.input.payload.last #= false
       for(idx <- 0 to 100){
          dut.clockDomain.waitRisingEdge()
          dut.input.valid #= nextBoolean()
          dut.input.payload.fragment.vec(0) #= nextInt(10)
          if(idx == 50){
              dut.input.payload.last #= true
          }
       }
    }
}