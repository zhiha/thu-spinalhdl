package test

import spinal.core._
import spinal.core.sim._

import breeze.math.Complex

import ffte.evaluate.cases.{Sim,FFTCase}
import ffte.evaluate.vectors.Cplx

import ffte.core.flat.{FIFFT,FFFT,Flat}
import ffte.core.streamed.{streamedComponents}

import ffte.property.{getShiftMethod,getFFTIW,getFFTOW,FFTIOConfig}

import ffte.algorithm.FFTGen
import scala.util.Random._

object FlatStudy extends Sim {
    var dW = getFFTIW()
    var oW = getFFTOW()
    def ce() = nextBoolean
    def sim(_inv:Boolean) : (Boolean,Int) = {
        inv = _inv
        S   = getShiftMethod(N) + (if(dW>oW) (dW-oW) else 0)
        var delay = 0
        val tv = test_vectors
        println(s"simulation $N gen test vectors ${getFFTIW()}")
            
        SimConfig.compile {
            val fft = if(inv) FIFFT(N) else FFFT(N)
            delay = fft.delay
            fft
        }.doSim{ dut =>
            done({
                dut.clockDomain.forkStimulus(period = 10)
            },{
                val tframe = idx%CC
                val rframe = (idx-delay+10*CC)%CC
                val d : Array[Cplx] = tv(tframe).d
                val q : Array[Cplx] = tv(rframe).q
                for(i <- 0 until N) {
                    dut.io.d.d(i).re.d.raw #= d(i).re
                    dut.io.d.d(i).im.d.raw #= d(i).im
                }
                dut.io.ce #= ce()
                dut.clockDomain.waitRisingEdge()
                if(dut.io.ce.toBoolean) {
                    if(idx>delay) {
                        for(i <- 0 until N) {
                            val r = Cplx(dut.io.q.d(i).re.d.raw.toInt,dut.io.q.d(i).im.d.raw.toInt)
                            if((r-q(i)).norm1>32) {
                                if(debug>2) {
                                    println(s"$rframe($i) $r != ${q(i)}, ${tv(rframe).d(i)}")
                                }
                                ook = false
                                ok  = false
                            }
                        }
                    }
                    if(!ook) failcnt += 1
                    next
                }
            })
        }
        (failcnt!=(C-delay),failcnt)
    }
    def singleTest(n:Int,k:Int,inv:Boolean):Unit = {
        debug = 3
        N = n
        K = k
        FFTGen.Winograd
        val (_ook,failcnt) = FFTIOConfig(dW,oW) on sim(inv)
        if(_ook) println(s"$n($inv): PASS($failcnt)") else println(s"$n: FAIL")
    }
    def caseTest(cases:Map[Int,Int],inv:Boolean): Unit = {
        K = 0
        debug = 1
        val nList = cases.map{ case(n,_) => n }.toList.sorted
        nList.map { n =>
            N = n
            println(s"start $N test")
            val (_ook,failcnt) = sim(inv)
            val msg = if(_ook) s"$n($inv): PASS($failcnt)" else s"$n: FAIL"
            Sim.log2file("tmp/flat.log",msg)
            (n,_ook,failcnt)
        }.foreach{case (n,oook,failcnt) =>
            if(oook) println(s"$n($inv): PASS($failcnt)") else println(s"$n: FAIL")
        }
    }
    def main(args: Array[String]) {
        C = 100
        ffte.default.core.GlobeSetting.evaluateNative = false
        val inv = if(args.length>0) (args(0).toInt==1) else false 
        if(args.length>1) {
            val n = args(1).toInt
            val k = args(2).toInt
            if(args.length>3) {
                dW = args(3).toInt
            }
            if(args.length>4) {
                oW = args(4).toInt
            }
            singleTest(n,k,inv)
        } else caseTest(FFTCase.basic,inv)
    }
}

object Flat2StreamedStudy extends Sim {
    var dW = getFFTIW()
    var oW = getFFTOW()
    def valid() = nextBoolean
    def ready() = nextBoolean
    def sim(_inv:Boolean) : (Boolean,Int) = {
        inv = _inv
        S   = getShiftMethod(N) + (if(dW>oW) (dW-oW) else 0)
        var delay = 0
        val tv = test_vectors
        println(s"simulation $N gen test vectors ${getFFTIW()}")
        val fp = if(rgen) (FFTGen.rgen[Flat](N)) else (FFTGen.gen[Flat](N)) 
        val tab     = fp.load_tab
        val out_tab = fp.store_tab
            
        SimConfig.withWave.compile {
            val m = new Flat(S)
            val fft = new m.toStreamed(fp.fft,"top").asInstanceOf[streamedComponents]
            delay = fft.delay
            fft
        }.doSim{ dut =>
            done({
                dut.clockDomain.forkStimulus(period = 10)
                dut.io.q.ready        #= true
                dut.io.d.payload.last #= true
                dut.clockDomain.waitRisingEdge()
                dut.io.d.payload.last #= false
            },{
                val tframe = idx%CC
                val rframe = (idx-delay+10*CC)%CC
                val d : Array[Cplx] = tv(tframe).d
                val q : Array[Cplx] = tv(rframe).q
                for(i <- 0 until N) {
                    dut.io.d.d(i).re.d.raw #= d(tab(i)).re
                    dut.io.d.d(i).im.d.raw #= d(tab(i)).im
                }
                if(tframe==CC-1) {
                    dut.io.d.payload.last #= true
                } else {
                    dut.io.d.payload.last #= false
                }
                dut.io.d.valid            #= valid()
                dut.io.q.ready            #= ready()
                dut.clockDomain.waitRisingEdge()
                if(dut.io.q.valid.toBoolean && dut.io.q.ready.toBoolean) {
                    if(idx>delay) {
                        for(i <- 0 until N) {
                            val ioq = dut.io.q.payload.fragment
                            val r = Cplx(ioq.d(i).re.d.raw.toInt,ioq.d(i).im.d.raw.toInt)
                            val cq = q(out_tab(i))
                            if((r-cq).norm1>32) {
                                if(debug>2) {
                                    println(s"$rframe($i) $r != ${cq}, ${tv(rframe).d(i)}")
                                }
                                ook = false
                                ok  = false
                            }
                        }
                        if(dut.io.q.payload.last.toBoolean) {
                            if(rframe!=CC-1) {
                                println(s"last flag not on ${CC-1}")
                            }
                        }
                    }
                    if(!ook) failcnt += 1
                    next
                }
            })
        }
        (failcnt!=(C-delay),failcnt)
    }
    def singleTest(n:Int,k:Int,inv:Boolean):Unit = {
        debug = 3
        N = n
        K = k
        FFTGen.Winograd
        val (_ook,failcnt) = FFTIOConfig(dW,oW) on sim(inv)
        if(_ook) println(s"$n($inv): PASS($failcnt)") else println(s"$n: FAIL")
    }
    def caseTest(cases:Map[Int,Int],inv:Boolean): Unit = {
        K = 0
        debug = 1
        val nList = cases.map{ case(n,_) => n }.toList.sorted
        nList.map { n =>
            N = n
            println(s"start $N test")
            val (_ook,failcnt) = sim(inv)
            val msg = if(_ook) s"$n($inv): PASS($failcnt)" else s"$n: FAIL"
            Sim.log2file("tmp/flat.log",msg)
            (n,_ook,failcnt)
        }.foreach{case (n,oook,failcnt) =>
            if(oook) println(s"$n($inv): PASS($failcnt)") else println(s"$n: FAIL")
        }
    }
    def main(args: Array[String]) {
        C = 100
        ffte.default.core.GlobeSetting.evaluateNative = false
        val inv = if(args.length>0) (args(0).toInt==1) else false 
        if(args.length>1) {
            val n = args(1).toInt
            val k = args(2).toInt
            if(args.length>3) {
                dW = args(3).toInt
            }
            if(args.length>4) {
                oW = args(4).toInt
            }
            singleTest(n,k,inv)
        } else caseTest(FFTCase.basic,inv)
    }
}