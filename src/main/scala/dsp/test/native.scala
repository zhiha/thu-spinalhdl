package test

import breeze.math.Complex

import ffte.evaluate.cases.{Sim,FFTCase}
import ffte.algorithm.FFTGen.{ffter,Winograd}
import ffte.native.{Native,NIFFT,NFFT}
import ffte.algorithm.FFTGen
import ffte.evaluate.vectors.Cplx

object NativeStudy extends Sim {
    def ci2c(x:Cplx) = Complex(x.re,x.im)

    def sim(fp:ffter[Native],_inv:Boolean) : (Boolean,Int) = {
        inv = _inv
        N = fp.rN
        val tv = test_vectors
        val fft = if(inv) new NIFFT(fp,S) else new NFFT(fp,S)
        done({
            failcnt = 0
        },{
            val frame = idx%CC
            val d : Array[Complex] = tv(frame).d.map(ci2c(_))
            val q : Array[Complex] = tv(frame).q.map(ci2c(_))
            val tq = fft.transform(d)
            for (i <- 0 until N) {
                val o = q(i)
                val r = tq(i)
                if((o-r).abs>32.0) {
                    ook = false
                    ok  = false
                    if(debug>0) {
                        println(s"$i: (out:$o right:$r)")
                    }
                }
                if(debug>2) {
                    println(s"$i: (out:$o right:$r)")
                }
            }
            if(!ook) failcnt += 1
            next
        })
        (failcnt!=C,failcnt)
    }
    def singleTest(n:Int,k:Int,inv:Boolean):Unit = {
        debug = 3
        val s = FFTCase.shift(n)
        S = s
        K = k
        Winograd
        val fp = if(rgen) FFTGen.rgen[Native](n) else FFTGen.gen[Native](n)
        val (ook,failcnt) = sim(fp,inv)
        if(ook) println(s"$n($inv): PASS($failcnt)") else println(s"$n: FAIL")
    }
    def caseTest(cases:Map[Int,Int],inv:Boolean): Unit = {
        K = 0
        debug = 1
        cases.map { case (n,s) =>
            S = s
            Winograd
            val fp = if(rgen) FFTGen.rgen[Native](n) else FFTGen.gen[Native](n)
            val (ook,failcnt) = sim(fp,inv)
            if(ook) println(s"$n($inv): PASS($failcnt)") else println(s"$n: FAIL")
            (n,ook,failcnt)
        }.foreach{case (n,ook,failcnt) =>
            if(ook) println(s"$n($inv): PASS($failcnt)") else println(s"$n: FAIL")
        }
    }
    def main(args: Array[String]) {
        ffte.default.core.GlobeSetting.evaluateNative = true
        if(args.length>0) {
            if(args(0).toInt==1) rgen = true 
        }
        val inv = if(args.length>1) (args(1).toInt==1) else false 
        if(args.length>2) {
            val n = args(2).toInt
            val k = args(3).toInt
            singleTest(n,k,inv)
        } else caseTest(FFTCase.cases,inv)
    }
}