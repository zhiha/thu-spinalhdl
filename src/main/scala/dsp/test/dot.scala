package test

import java.io.FileWriter

import ffte.algorithm.FFTGen
import ffte.dot.Dot

object DotStudy {
    def main(args: Array[String]) {
        FFTGen.Winograd
        val n = args(0).toInt
        val m = new Dot
        val g = FFTGen.gen[Dot](n)
        val o = m.graph(g.fft)
        val out = new FileWriter(s"tmp/dft$n.dot",false)
        out.write(o+"\n")
        out.close()
    }
}