package dsp.basic.misc

import dsp._
import dsp.basic.types._
import spinal.core._
import breeze.numerics._

class MatExpand(peekWidth:Int,resolution:Int,dim:Int,expandNum:Int) extends Component{
    val io = new Bundle{
        val inputMat = in(matSF(peekWidth,resolution,dim))
        val outputMat = out(matSF(peekWidth,resolution,dim+expandNum))
    }
    for(i <- 0 until dim+expandNum){
        for(j <- 0 until dim+expandNum){
            if(i==j && i<expandNum){
                io.outputMat.mat(i).vec(j) := 1.0
            }else if(i<expandNum){
                io.outputMat.mat(i).vec(j) := 0.0
            }else if(j<expandNum){
                io.outputMat.mat(i).vec(j) := 0.0
            }else{
                io.outputMat.mat(i).vec(j) := io.inputMat.mat(i-expandNum).vec(j-expandNum)
            }
        }
    }
}