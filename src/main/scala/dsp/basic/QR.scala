package dsp.basic

import dsp.basic.types._
import spinal.core._
import breeze.numerics._
import dsp.basic.misc._


class QR(peekWidth:Int,resolution:Int,dim:Int) extends Component{
    val io = new Bundle{
        val ready = out(Bool())
        val valid = in(Bool())
        val input = in(matSF(peekWidth,resolution,dim))
        val outputR = out(matSF(peekWidth,resolution,dim))
        val outputQ = out(matSF(peekWidth,resolution,dim))
        val outvalid = out(Bool())
    }
    val regR = Reg(matSF(peekWidth,resolution,dim)) init(matSF.zeroMat(peekWidth,resolution,dim))
    val regQ = Reg(matSF(peekWidth,resolution,dim)) init(matSF.eyeMat(peekWidth,resolution,dim))
    val stage= Reg(UInt(4 bits)) init(0)
    val ready = Reg(Bool()) init(true)
    val outvalid = Reg(Bool()) init(false)
    val start = Reg(Bool()) init(false)

    
    io.ready := ready
    io.outputQ := regQ
    io.outputR := regR
    io.outvalid := outvalid

    // input data catch hand-shake
    when(io.valid && ready && (start===False)){
        start := True
        ready := False
        regR := io.input
        regQ := matSF.eyeMat(peekWidth,resolution,dim)
    }.otherwise{
        ready := ready
    }
    when(outvalid){
        io.outputQ := regQ
        io.outputR := regR
        outvalid := False
        ready := True
    }
    
    for(i <- 0 until dim-1){
        val cal = new Area{
            val vecData = regR.mat(i).from(i,dim-1)
            val cnt = Reg(UInt(4 bits)) init(0)
            val valid = Reg(Bool()) init(false)
            val first = Reg(Bool()) init(true)
            val go = Reg(Bool()) init(false)
            val regExp = Reg(matSF(peekWidth,resolution,dim-i)) init(matSF.zeroMat(peekWidth,resolution,dim-i))
            val hMat = Reg(matSF(peekWidth,resolution,dim)) init(matSF.zeroMat(peekWidth,resolution,dim))
            val householder = new HouseHolder(peekWidth,resolution,dim-i)
            val expandMat = new MatExpand(peekWidth,resolution,dim-i,i)
            expandMat.io.inputMat := regExp
            householder.io.data := vecData
            householder.io.valid := valid
            valid := False
            when(stage === U(i) && start){
                when(householder.io.ready && first){
                    when(cnt===U(3)){
                        cnt := 0
                        householder.io.data := vecData
                        valid := True
                        first := False
                    }.otherwise{
                        cnt := cnt + 1
                    }
                }
                when(householder.io.outvalid){
                    go := True
                    regExp := householder.io.outputH
                    expandMat.io.inputMat := regExp
                }
                hMat := expandMat.io.outputMat
                when(go){
                    when(cnt===U(7)){
                        regR := hMat * regR
                        regQ := regQ * hMat
                        go := False
                        cnt := 0
                        first := True
                        when(stage===U(dim-2)){
                            stage := 0
                            outvalid := True
                            start := False
                        }.otherwise{
                            stage := stage + 1
                        }
                    }.otherwise{
                        cnt := cnt + 1
                    }
                }
            }
        }
    }
    
}