package dsp.basic

import dsp._
import dsp.basic.types._
import spinal.core._
import breeze.numerics._

class HouseHolder(peekWidth:Int,resolution:Int,dim:Int) extends Component{
    val io = new Bundle{
        val data = in(vecSF(peekWidth,resolution,dim))
        val ready = out(Bool())
        val valid = in(Bool())
        val outvalid = out(Bool())
        val outputH = out(matSF(peekWidth,resolution,dim))
    }
    val regNorm = Reg(SFix(peekWidth exp,-resolution exp)) init(0.0)
    val regData = Reg(vecSF(peekWidth,resolution,dim)) init(vecSF.zeroVec(peekWidth,resolution,dim))
    val stage= Reg(UInt(4 bits)) init(0)
    val ready = Reg(Bool()) init(true)
    val outvalid = Reg(Bool()) init(false)

    regData := io.data
    io.ready := ready
    

    // stage 0
    val go = Reg(Bool()) init(false)
    val idx = Reg(UInt(log2Up(dim) bits)) init(0)
    val cnt = Reg(UInt(6 bits)) init(U(0))
    val norm = Reg(SFix(peekWidth exp, -resolution exp)) init(0.0) 
    val cordic = new Cordic(peekWidth,resolution,Config(VECTOR,CIRCULAR))
    val divide = new Cordic(peekWidth,resolution,Config(VECTOR,LINEAR))
    cordic.io.input.x := 0.0
    cordic.io.input.y := 0.0
    cordic.io.input.z := 0.0
    divide.io.input.x := 2.0
    divide.io.input.y := 1.0
    divide.io.input.z := 0.0
    // stage 1
    val w = Reg(vecSF(peekWidth,resolution,dim)) init(vecSF.zeroVec(peekWidth,resolution,dim))
    val eyeVec = Reg(vecSF(peekWidth,resolution,dim)) 
    eyeVec := vecSF.eyeVec(peekWidth,resolution,dim)
    // stage 2
    val wNorm = Reg(SFix(peekWidth exp, -resolution exp)) init(0.0) 
    // stage 3
    val scale = Reg(SFix(peekWidth exp, -resolution exp)) init(0.0) 
    // stage 4
    val hMat = Reg(matSF(peekWidth,resolution,dim)) init(matSF.zeroMat(peekWidth,resolution,dim))
    val eyeMat = Reg(matSF(peekWidth,resolution,dim)) 
    eyeMat := matSF.eyeMat(peekWidth,resolution,dim)
    io.outputH := hMat
    io.outvalid := outvalid
    // input data catch hand-shake
    when(io.valid && ready){
        regData := io.data
        go := True
        ready := False
    }.otherwise{
        regData := regData
        ready := ready
    }
    switch(stage){
        is(U(0)){
            outvalid := False
            when(go){
                when(idx < U(dim-1)){
                    when(idx === U(0)){
                        cordic.io.input.x := regData.vec(idx).truncated
                        cordic.io.input.y := regData.vec(idx+1).truncated
                    }.otherwise{
                        when(idx =/= U(dim-1)){
                            cordic.io.input.x := cordic.io.output.x.truncated
                            cordic.io.input.y := regData.vec(idx+1)
                        }
                    }    
                    when(cnt === U(19)){
                        cnt := 0 
                        idx := idx + 1
                        when(idx === U(dim-2)){
                            norm := cordic.io.output.x.truncated
                            idx := 0
                            stage := stage + 1
                        }
                    }.otherwise{
                        cnt := cnt + 1
                    }
                }
            }
        }
        is(U(1)){
            go := False
            w := Mux(regData.vec(0)>0.0,regData + eyeVec * norm,regData - eyeVec * norm) // w = x + sign(x1)||x||_2*e1
            when(cnt === U(2)){
                cnt := 0 
                stage := stage + 1
            }.otherwise{
                cnt := cnt + 1
            }
        }
        is(U(2)){
            when(idx < U(dim-1)){
                when(idx === U(0)){
                    cordic.io.input.x := w.vec(idx).truncated
                    cordic.io.input.y := w.vec(idx+1).truncated
                }.otherwise{
                    when(idx =/= U(dim-1)){
                        cordic.io.input.x := cordic.io.output.x.truncated
                        cordic.io.input.y := w.vec(idx+1)
                    }
                }    
                when(cnt === U(19)){
                    cnt := 0 
                    idx := idx + 1
                    when(idx === U(dim-2)){
                        wNorm := cordic.io.output.x.truncated
                        idx := 0
                        stage := stage + 1
                    }
                }.otherwise{
                    cnt := cnt + 1
                }
            }
        }
        is(U(3)){
            divide.io.input.x := (wNorm*wNorm).truncated
            divide.io.input.y := 1.0
            divide.io.input.z := 0.0
            when(cnt === U(19)){ 
                scale := divide.io.output.z.truncated
                cnt := 0 
                stage := stage + 1
            }.otherwise{
                cnt := cnt + 1
            }
        } 
        is(U(4)){
            hMat := eyeMat - (w*(scale<<1)*w)
            stage := 0
            ready := True
            outvalid := True
        }
    }

}