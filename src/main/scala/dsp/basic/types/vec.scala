package dsp.basic.types

import dsp._
import spinal.core._
import breeze.numerics._

case class vecSF(peekWidth:Int,resolution:Int,dim:Int) extends Bundle{
    val vec = Vec(SFix(peekWidth exp, -resolution exp),dim)

    def +(vec:vecSF):vecSF={
        val ret = vecSF(peekWidth,resolution,dim)
        for(i <- 0 until dim) {
            ret.vec(i) := this.vec(i) + vec.vec(i)
        }
        ret
    }
    def -(vec:vecSF):vecSF={
        val ret = vecSF(peekWidth,resolution,dim)
        for(i <- 0 until dim) {
            ret.vec(i) := this.vec(i) - vec.vec(i)
        }
        ret
    }
    def *(vec:vecSF):matSF={
        val ret = matSF(peekWidth,resolution,dim)
        for(i <- 0 until dim){
            for(j <- 0 until dim){
                ret.mat(i).vec(j) := (this.vec(i) * vec.vec(j)).truncated
            }
        }
        ret
    }
    def *(sf:SFix):vecSF={
        val ret = vecSF(peekWidth,resolution,dim)
        for(i <- 0 until dim) {
            ret.vec(i) := (this.vec(i) * sf).truncated
        }
        ret
    }
    def dot(vec:vecSF):SFix={
        val ret = vecSF(peekWidth,resolution,dim)
        for(i <- 0 until dim){
            ret.vec(i) := ( this.vec(i) * vec.vec(i) ).truncated 
        }
        ret.vec.reduce(_+_).truncated
    }
    /*
    def norm(ready:Bool) = {
        val idx = Reg(UInt(log2Up(dim) bits)) init(0)
        val cnt = Reg(UInt(6 bits)) init(U(0))
        val inNorm = Reg(SFix(peekWidth exp, -resolution exp)) init(0.0) 
        val cordic = new Cordic(peekWidth,resolution,Config(VECTOR,CIRCULAR))
        cordic.io.input.x := 0.0
        cordic.io.input.y := 0.0
        cordic.io.input.z := 0.0
        when(ready){
            when(idx < U(dim-1)){
                when(idx === U(0)){
                    cordic.io.input.x := this.vec(idx).truncated
                    cordic.io.input.y := this.vec(idx+1).truncated
                }.otherwise{
                    when(idx =/= U(dim-1)){
                        cordic.io.input.x := cordic.io.output.x.truncated
                        cordic.io.input.y := this.vec(idx+1)
                    }
                }    
                when(cnt === U(19)){
                    cnt := 0 
                    when(idx === U(dim-2)){
                        inNorm := cordic.io.output.x.truncated
                        idx := 0
                    }
                    idx := idx + 1
                }.otherwise{
                    cnt := cnt + 1
                }
            }
        }
        (idx===U(dim-1),inNorm)
    }
    def norm1(rdy:Bool) = {
        val idx = Reg(UInt(log2Up(dim) bits)) init(0)
        val cnt = Reg(UInt(6 bits)) init(U(0))
        val start = Reg(Bool()) init(true)
        val inNorm = Reg(SFix(peekWidth exp, -resolution exp)) init(0.0) 
        val cordic = new Cordic(peekWidth,resolution,Config(VECTOR,CIRCULAR))
        cordic.io.input.x := 0.0
        cordic.io.input.y := 0.0
        cordic.io.input.z := 0.0
        when(rdy){
            when(idx < U(dim-1)){
                start := False
                when(idx === U(0)){
                    cordic.io.input.x := this.vec(idx).truncated
                    cordic.io.input.y := this.vec(idx+1).truncated
                }.otherwise{
                    when(idx =/= U(dim-1)){
                        cordic.io.input.x := cordic.io.output.x.truncated
                        cordic.io.input.y := this.vec(idx+1)
                    }
                }    
                when(cnt === U(19)){
                    cnt := 0 
                    idx := idx + 1
                    when(idx === U(dim-2)){
                        inNorm := cordic.io.output.x.truncated
                        idx := 0
                    }
                }.otherwise{
                    cnt := cnt + 1
                }
            }
        }
        (RegNext(((cnt === U(19))&&(idx===U(dim-2)))) || start,inNorm)        
    }
    def houseHolder(ready:Bool) = {
        val ret = Reg(matSF(peekWidth,resolution,dim)) init(matSF.zeroMat(peekWidth,resolution,dim))
        val cnt = Reg(UInt(8 bits)) init(U(0))
        val householder = new HouseHolderDev(peekWidth,resolution,dim)
        householder.io.input := vecSF.zeroVec(peekWidth,resolution,dim)
        householder.io.ready := ready
        when(ready){
            householder.io.input := this
            householder.io.ready := ready
            when(cnt === U(101)){
                ret := householder.io.outputH
            }.otherwise{
                cnt := cnt + 1
            }
        }
        (cnt===U(101),ret)
    }
    */
    def abs = {
        val ret = vecSF(peekWidth,resolution,dim)
        for(i <- 0 until dim) {
            ret.vec(i) := Mux(this.vec(i)>0,this.vec(i),this.vec(i))
        }
        ret
    }
    def from(a:Int,b:Int) = {
        val ret = vecSF(peekWidth,resolution,b-a+1)
        for(i <- 0 until (b-a+1)) {
            ret.vec(i) := this.vec(a+i)
        }
        ret
    }
    override def toString:String={
        var str = ""
        for(i <- 0 until dim){
            str = str + "%.3f\t".format(this.vec(i).toDouble)
        }
        str = str + "\n"
        str
    }
}

object vecSF{
    def eyeVec(peekWidth:Int,resolution:Int,dim:Int):vecSF={
        val ret = vecSF(peekWidth,resolution,dim)
        for(i <- 0 until dim) {
            if(i==0) ret.vec(i) := 1.0 else ret.vec(i) := 0.0
        }
        ret
    }
    def zeroVec(peekWidth:Int,resolution:Int,dim:Int):vecSF={
        val ret = vecSF(peekWidth,resolution,dim)
        for(i <- 0 until dim) {
            ret.vec(i) := 0.0
        }
        ret
    }
}
