package dsp.basic.types

import dsp._
import spinal.core._
import breeze.numerics._

//vec is col vector
case class matSF(peekWidth:Int,resolution:Int,dim:Int) extends Bundle{
    val mat = Vec(vecSF(peekWidth,resolution,dim),dim)
    def -(mat:matSF):matSF={
        val ret = matSF(peekWidth,resolution,dim)
        for(i <- 0 until dim) {
            for(j <- 0 until dim){
                ret.mat(i).vec(j) := this.mat(i).vec(j) - mat.mat(i).vec(j)
            }
        }
        ret
    }
    def *(mat:matSF):matSF={
        val ret = matSF(peekWidth,resolution,dim)
        for(i <- 0 until dim){
            for(j <- 0 until dim){
                ret.mat(i).vec(j) := (this.transpose.mat(j).dot(mat.mat(i))).truncated 
            }
        }
        ret
    }
    def transpose={
        val ret = matSF(peekWidth,resolution,dim)
        for(i <- 0 until dim){
            for(j <- 0 until dim){
                ret.mat(i).vec(j) := this.mat(j).vec(i)
            }
        }
        ret
    }
    def expand={
        val ret = matSF(peekWidth,resolution,dim+1)
        for(i <- 0 until dim+1){
            for(j <- 0 until dim+1){
                if(i==0 && j==0){
                    ret.mat(i).vec(j) := 1.0
                }else if(i==0){
                    ret.mat(i).vec(j) := 0.0
                }else if(j==0){
                    ret.mat(i).vec(j) := 0.0
                }else{
                    ret.mat(i).vec(j) := this.mat(i-1).vec(j-1)
                }
            }
        }
        ret
    }
    override def toString={
        var str:String = ""
        for(i <- 0 until dim){
            for(j <- 0 until dim){
                str = str + "%.3f\t".format(this.mat(j).vec(i).toDouble)
            }
            str = str + "\n"
        }
        str
    }
}
object matSF{
    def eyeMat(peekWidth:Int,resolution:Int,dim:Int):matSF={
        val ret = matSF(peekWidth,resolution,dim)
        for(i <- 0 until dim){  // row
            for(j <- 0 until dim){ //col
                if(i==j) ret.mat(i).vec(j) := 1.0 else ret.mat(i).vec(j) := 0.0
            }
        }
        ret
    }
    def zeroMat(peekWidth:Int,resolution:Int,dim:Int):matSF={
        val ret = matSF(peekWidth,resolution,dim)
        for(i <- 0 until dim){  // row
            for(j <- 0 until dim){ //col
                ret.mat(i).vec(j) := 0.0
            }
        }
        ret
    }
}