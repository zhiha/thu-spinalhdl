package dsp.basic

import dsp._
import spinal.core._
import breeze.numerics._


trait SystemConfig
case object CIRCULAR extends SystemConfig
case object LINEAR extends SystemConfig

trait ModeConfig
case object ROTATE extends ModeConfig
case object VECTOR extends ModeConfig

case class Config(mode:ModeConfig,system:SystemConfig) 

case class CordicData(peekWidth:Int,resolution:Int) extends Bundle{
    val x: SFix = SFix(peekWidth exp, -resolution exp) // 1QN
    val y: SFix = SFix(peekWidth exp, -resolution exp) // 1QN
    val z: SFix = SFix(2 exp, -resolution exp) // 2QN
}

class Cordic(peekWidth:Int,resolution:Int,config:Config) extends Component{
    val io = new Bundle{
        val input = in(CordicData(peekWidth,resolution))
        val output = out(CordicData(peekWidth,resolution))
    }

    val regX = Reg(Vec((0 to 17).map(i => SFix(peekWidth exp, -(resolution + i) exp))))
    val regY = Reg(Vec((0 to 17).map(i => SFix(peekWidth exp, -(resolution + i) exp))))
    val regZ = Reg(Vec((0 to 17).map(i => SFix(peekWidth exp, -(resolution + i) exp)))) 
    val signX = Reg(UInt(18 bits)) init(0)
    val signY = Reg(UInt(18 bits)) init(0)

    val scale = SF((0 to 17).map(i => cos(atan(pow(2.0,-i)))).product,1 exp, - (resolution+16) exp)
    val phaseData = (0 to 17).map(i => SF(atan(pow(2.0,-i)), 2 exp, -(resolution + i) exp))
    val divideData = (0 to 17).map(i => SF(pow(2.0,-i), 2 exp, -(resolution + i) exp))
    val piData = SFix(peekWidth exp, -(resolution + 17) exp) 
    piData := math.Pi


    val di  = config.mode match {
        case ROTATE => regZ.map(x => ~x.asBits.msb)
        case VECTOR => regY.map(x => x.asBits.msb)
    }
    

    (0 to 17).foreach{ i =>
        if (i==0) {
            regX(i) := io.input.x.abs.truncated
            regY(i) := io.input.y.truncated
            regZ(i) := io.input.z.truncated
            signX(i) := Mux(io.input.x.raw.msb,False,True)
            signY(i) := Mux(io.input.y.raw.msb,False,True)
        }
        else if(i==17){
            config.system match {
                case CIRCULAR => {
                    regX(i) := (regX(i-1) * scale).truncated
                    regY(i) := (regY(i-1) * scale).truncated
                    regZ(i) := regZ(i-1).truncated
                }
                case LINEAR => {
                    regX(i) := (regX(i-1)).truncated
                    regY(i) := (regY(i-1)).truncated
                    regZ(i) := regZ(i-1).truncated                   
                }
            }
            signX(i) := signX(i-1)
            signY(i) := signY(i-1)
        }
        else{
            config.system match {
                case CIRCULAR => {
                    regX(i) := Mux(di(i-1),regX(i-1)-(regY(i-1)>>(i-1)),regX(i-1)+(regY(i-1)>>(i-1))).truncated
                    regY(i) := Mux(di(i-1),regY(i-1)+(regX(i-1)>>(i-1)),regY(i-1)-(regX(i-1)>>(i-1))).truncated
                    regZ(i) := Mux(di(i-1),regZ(i-1) - phaseData(i-1),regZ(i-1) + phaseData(i-1)).truncated
                }
                case LINEAR => {
                    regX(i) := regX(i-1).truncated
                    regY(i) := Mux(di(i-1),regY(i-1)+(regX(i-1)>>i),regY(i-1)-(regX(i-1)>>i)).truncated
                    regZ(i) := Mux(di(i-1),regZ(i-1)-divideData(i),regZ(i-1)+divideData(i)).truncated
                }
            }
            signX(i) := signX(i-1)
            signY(i) := signY(i-1)
        }
    }

    
    io.output.x := regX(17).truncated
    io.output.y := regY(17).truncated
    io.output.z := Mux(signX(17),regZ(17),Mux(signY(17),piData - regZ(17),regZ(17).abs-piData)).truncated  //piData - regZ(17),regZ(17)).truncated//regZ(17).truncated

    val latency = 19

}