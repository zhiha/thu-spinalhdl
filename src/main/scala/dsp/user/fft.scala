package dsp.user

import spinal.core._
import spinal.lib._
import ffte.core._
import ffte.property.{getShiftMethod,getFFTIW,getFFTOW,FFTIOConfig}
import ffte.algorithm.FFTGen.{combineFT,Method}
import ffte.types.{VecComplex}
import ffte.default.config
import ffte.algorithm.FFTGen
import ffte.core.streamed.{Streamed,streamedComponents}

class FFT(val N:Int, val dW:Int, val oW:Int, val G:Int) extends Component{
    val io = new Bundle{
        val d = slave(StreamedBus(VecComplex(1,dW)))
        val q = master(StreamedBus(VecComplex(1,oW)))
        val ce = out(Bool())
    }
    
    val fft = FFTIOConfig(dW,oW) on new Area {
        val default_dW = getFFTIW()
        val default_oW = getFFTOW()
        val fp = FFTGen.gen[Streamed](N)
        val loadTab = fp.load_tab
        val storeTab = fp.store_tab
        val S  = getShiftMethod(N) + (if(default_dW>default_oW) (default_dW-default_oW) else 0)
        val fft = fp.gen(new Streamed(S)).asInstanceOf[streamedComponents]
    }

    val memLoad = Mem(VecComplex(1,dW),N)
    val memStore = Mem(VecComplex(1,dW),N)
    val romLoad = Mem(UInt(log2Up(N) bits),initialContent = loadTable)
    val romStore = Mem(UInt(log2Up(N) bits),initialContent = storeTable)

    val transform1 = new Area{
        val wgo = Reg(Bool()) init(true)
        val rgo = Reg(Bool()) init(false)
        val last = Reg(Bool()) init(false)
        val valid = Reg(Bool()) init(false)
        val wCnt = Counter(N,inc= wgo && io.d.valid) 
        val rCnt = Counter(N,inc= rgo && fft.fft.io.d.fire) 
        val sign = Reg(Bool()) init(false)
        val sCnt = Counter(2,inc=sign) 

        when(wCnt.willOverflow){
            rCnt.clear
            wgo := False
            rgo := True
            last := True
        }
        when(rCnt.willOverflow){
            wCnt.clear
            rgo := False
            wgo := True
            last := False
        }
    
        memLoad.write(
            address = wCnt,
            data    = io.d.payload.fragment,
            enable  = wgo 
        )
        fft.fft.io.d.payload.fragment := memLoad.readSync(
            address = romLoad.readSync(rCnt),
            enable  = rgo
        )
        
        when(rgo){
            when(sign===False){
                valid := True
            }.elsewhen(sCnt.willOverflow){
                valid := True
                sign := False
                sCnt.clear()
            }
            when(fft.fft.io.d.fire){
                valid := False
                sign := True
            }
        }
        io.d.ready := wgo

        fft.fft.io.d.valid := valid

        fft.fft.io.d.payload.last := Delay(rCnt === U(N-1) || (last && wgo),2)
    }
    

    val transform2 = new Area{
        val qready = Reg(Bool()) init(true)
        val startCount = Reg(UInt(log2Up(delay)+1 bits)) init(0)
        val start = Reg(Bool()) init(false)
        val waitDelay = Reg(UInt(2 bits)) init(0)
        val writeEn = Reg(Bool()) init(true)
        val writeCnt = Counter(N,inc= fft.fft.io.q.fire && start)
        val readEn = Reg(Bool()) init(false)
        val readCnt = Counter(N,inc= io.q.fire)
        val qvalid = Reg(Bool()) init(false)
        fft.fft.io.q.ready := qready
        when(fft.fft.io.d.fire){
            startCount := startCount + 1
        }
        when(startCount === U(delay)){
            start := True
        }
        when(writeEn){
            when(fft.fft.io.q.fire){
                qready := False
            }
            when(qready===False){
                waitDelay := waitDelay + 1
                when(waitDelay === U(1)){
                    qready := True
                    waitDelay := 0
                }
            }
        }.otherwise{
            qready := False
        }
        when(readEn){
            qvalid := True
            when(io.q.ready){
                qvalid := False
            }
        }otherwise{
            qvalid := False
        }
        when(writeCnt.willOverflow){
            readCnt.clear
            writeEn := False
            readEn := True
        }
        when(readCnt.willOverflow){
            writeCnt.clear
            writeEn := True
            readEn := False
            qready := True
        }
        memStore.write(
            address = romStore.readAsync(writeCnt),
            data    = fft.fft.io.q.payload.fragment,
            enable  = writeEn && fft.fft.io.q.fire && start
        )   
        io.q.payload.fragment := memStore.readSync(
            address = readCnt,
            enable  = readEn
        )
        io.q.valid := readEn && qvalid
        io.q.payload.last := Delay(readCnt === U(N-1) || writeEn, 1)
        
    }

    io.ce := io.q.fire

    def loadTable = for(i <- 0 until N) yield {
        U(fft.loadTab(i),log2Up(N) bits)
    }

    def storeTable = for(i <- 0 until N) yield {
        U(fft.storeTab(i),log2Up(N) bits)
    }

    def delay = fft.fft.delay

}