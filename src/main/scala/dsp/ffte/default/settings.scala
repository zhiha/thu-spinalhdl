package ffte.default

import ffte.evaluate.vectors.{DirectVector,NativeVector}
object core {
    object GlobeSetting {
        var evaluateNative = false
        def testvector(N:Int,S:Int,inv:Boolean) = 
            if(evaluateNative) DirectVector(N,S,inv)
            else NativeVector(N,S,inv)
    }
}