package ffte.default

import ffte.property.{FFTGlobeConfig,FFTIOConfig,FFTRTLConfig,getFFTScalaFactor}
import ffte.core.StreamedGroupConfig
object config {
    val DefaultFFTGlobeConfig  = FFTGlobeConfig(
        1.2
    )
    val DefaultFFTGenRTLConfig = FFTRTLConfig(
          true
        , true
        , -15
        , -17
        , {x => Math.round(Math.log(x)/Math.log(2)/getFFTScalaFactor()).toInt}
    )
    val DefaultFFTIOConfig = FFTIOConfig(
          18
        , 18
    )
    val DefaultStreamedGroupConfig = StreamedGroupConfig(1)
}