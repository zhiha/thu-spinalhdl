package ffte.algorithm

import breeze.math.Complex

object FFTMisc {
    def wnk(k:Int, n:Int) = Complex(Math.cos(2*Math.PI*k/n),-Math.sin(2*Math.PI*k/n))
    def dft(x:List[Complex]) = {
		val n = x.length
		(0 until n).map{i => x.zipWithIndex.map{ case (a,k) => a*wnk(i*k,n)}.sum}.toList
	}
    def idft(x:List[Complex]) = {
		val n = x.length
		(0 until n).map{i => x.zipWithIndex.map{ case (a,k) => a*wnk(-i*k,n)}.sum}.toList
	}
    def dft(x:Array[Complex]) = {
		val n = x.length
		(0 until n).map{i => x.zipWithIndex.map{ case (a,k) => a*wnk(i*k,n)}.sum}.toArray
	}
    def idft(x:Array[Complex]) = {
		val n = x.length
		(0 until n).map{i => x.zipWithIndex.map{ case (a,k) => a*wnk(-i*k,n)}.sum}.toArray
	}
    def pow(n:Int,k:Int):BigInt = if(k==0) 1 else n*pow(n,k-1)
    object log2Up {
        def apply(value: BigInt): Int = {
            assert(value > 0,s"No negative value ($value) on ${this.getClass.getSimpleName}") 
            (value - 1).bitLength
        }
    }
}