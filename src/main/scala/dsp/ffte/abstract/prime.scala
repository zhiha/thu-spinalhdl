package ffte.algorithm

object Prime {
    // Generate the primes list
    lazy val primes: Stream[Int] = 2 #:: Stream.from(3).filter(i => primes.takeWhile(j => j * j <= i).forall(i % _ > 0))
    // Extract the first 16384 prime numbers
    val primeList = primes.take(16384).toList

    def split(i:Int,n:Int,p:Int):(Int,Int) = if(i%p==0) split(i/p,n+1,p) else (i,n)
    def repesent(N:Int) = (for(p <- primeList; if (N%p==0) ) yield {
        val (_,n) = split(N,0,p)
        (p,n)
    }).toMap

    // Find the gcd between a and b
    def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b)

    // Judge whether the prime
    def isPrime(a: Int): Boolean = primeList.contains(a)

    case class Base(first:Int,second:Int) {
        val N = first*second
        val invsecond = BigInt(second).modInverse(BigInt(first)).toInt
        val invfirst  = second-(invsecond*second-1)/first
        def rem(n:Int) = (n%first,n%second)
        def chinese(f:Int,s:Int) = (s*invfirst*first+f*invsecond*second)%N
    }
}