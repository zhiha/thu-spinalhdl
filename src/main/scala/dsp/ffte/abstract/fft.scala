package ffte.algorithm

object FFTGen {
    trait genericFT {
        def N : Int
    }

    case class DFT(val N:Int) extends genericFT {
        override def toString = s"dft_$N"
    }

    trait combineFT[T<:Method] extends genericFT {
        val p  : ffter[T]
        val ib = p.first.fft
        val ob = p.second.fft
    }

    case class psbFFT[T<:Method](p:ffter[T]) extends combineFT[T] {
        val N  = p.rN
        override def toString = s"psb f_$ib,s_$ob"
    }

    case class msbFFT[T<:Method](p:ffter[T]) extends combineFT[T] {
        val N  = p.rN
        override def toString = s"msb f_$ib,s_$ob"
    }

    case class wpbFFT[T<:Method](val N:Int,p:ffter[T]) extends combineFT[T] {
        override def toString = s"wino f_$ib,s_$ob"
    }

    trait Method {
        def apply(entity:genericFT,prefix:String) : Any
    }
    
    trait ffter[T<:Method] {
        def first   : ffter[Method]
        def second  : ffter[Method]
        def rN : Int = first.rN * second.rN
        def fft  : genericFT 
        val load_tab  : Array[Int] 
        val store_tab : Array[Int]
        def load(f:Int,s:Int)  = f*second.rN+s
        def store(f:Int,s:Int) = f+s*second.rN
        def gen(m:T) = m(fft,"top")
        def rgen : Boolean
        def tab_dump = load_tab.map(_.toString).reduce(_+","+_)+"|"+store_tab.map(_.toString).reduce(_+","+_)
    }

    case object fft1 extends ffter[Method] {
        val first  = fft1
        val second = fft1
        override def rN = 1
        def fft: genericFT = DFT(1)
        val load_tab: Array[Int] = Array(0)
        val store_tab: Array[Int] = Array(0)
        val rgen = false
    }

    case class pb[T<:Method](N:Int,rgen:Boolean) extends ffter[T] {
        assert(Prime.isPrime(N),s"pb need $N is prime")
        val first   = this.asInstanceOf[ffter[Method]]
        val second  = fft1
        override def rN = N
        def fft = DFT(N)
        override def load(p:Int,s:Int)  = p
        override def store(p:Int,s:Int) = p
        override def toString = s"prime fft $N"
        val load_tab: Array[Int] = (for(i<- 0 until N) yield i).toArray
        val store_tab: Array[Int] = (for(i<- 0 until N) yield i).toArray
    }

    case class psb[T<:Method](first:ffter[Method],second:ffter[Method],rgen:Boolean) extends ffter[T] {
        val N = rN
        assert(Prime.gcd(first.rN,second.rN)==1,"psb need ${n.N} and ${m.N} coprime")
        def fft = psbFFT(this)
        val base = Prime.Base(first.rN,second.rN)
        val load_tab: Array[Int] = 
                if(rgen) 
                    (for(s <- 0 until second.rN;f <- 0 until first.rN) yield 
                        (second.load_tab(s)*first.rN+first.load_tab(f)*second.rN)%N).toArray 
                else
                    (for(s <- 0 until second.rN; f <- 0 until first.rN) yield 
                        base.chinese(first.load_tab(f),second.load_tab(s))).toArray 
        val store_tab: Array[Int] = 
                if(rgen)
                    (for(f <- 0 until first.rN; s <- 0 until second.rN) yield 
                        base.chinese(first.store_tab(f),second.store_tab(s))).toArray 
                else
                    (for(f <- 0 until first.rN;s <- 0 until second.rN) yield 
                        (second.store_tab(s)*first.rN+first.store_tab(f)*second.rN)%N).toArray 
        override def toString = s"psb($rgen): $first|$second"
    }

    case class msb[T<:Method](first:ffter[Method],second:ffter[Method],rgen:Boolean) extends ffter[T] {
        val N = rN
        def fft  = msbFFT(this)
        val load_tab: Array[Int] = if(rgen) 
                (for(f <- 0 until first.rN;s <- 0 until second.rN)  yield 
                    first.load_tab(f) + second.load_tab(s)*first.rN).toArray
            else 
                (for(s <- 0 until second.rN; f <- 0 until first.rN) yield 
                    first.load_tab(f)*second.rN + second.load_tab(s)).toArray 
        val store_tab: Array[Int] = if(rgen)
                (for(s <- 0 until second.rN;f <- 0 until first.rN) yield 
                    (second.store_tab(s)+first.store_tab(f)*second.rN)%N).toArray
            else
                (for(f <- 0 until first.rN;s <- 0 until second.rN) yield 
                    (second.store_tab(s)*first.rN+first.store_tab(f))%N).toArray
        override def toString = s"msb($rgen): $first|$second"
    }

    case class wpbp[T<:Method](N:Int,first:ffter[Method],second:ffter[Method],rgen:Boolean) extends ffter[T] {
        assert(Prime.isPrime(N),s"wpbp need $N is prime")
        val oc = circularOrder(N)
        override def rN = N
        
        def fft = wpbFFT(N,this)
        override def toString = s"winograd prime fft($rgen) $N " 
        val load_tab: Array[Int] = (for(f <- 0 until N) yield if(f==0) 0 else oc.tab(first.load_tab(f-1)+1)).toArray 
        val store_tab : Array[Int] = (for(s <- 0 until N) yield if(s==0) 0 else oc.tab(second.store_tab(s-1)+1)).toArray 
    }

    var wMethod = false
    def Winograd :Unit   = { wMethod = true }
    def FlattenDFT :Unit = { wMethod = false }

    
    def choose_factor(n:Int) : Int = {
        val rn = Prime.repesent(n).toList
        if(rn.size==1) {
            if(rn(0)._1==2 && rn(0)._2%2==0 && rn(0)._2>2) FFTMisc.pow(4,rn(0)._2/4).toInt
            else FFTMisc.pow(rn(0)._1,rn(0)._2/2).toInt
        } else {
            val np = rn.map{case (k,v) => FFTMisc.pow(k,v)}.sortWith(_>_)
            def check(a:BigInt,b:List[BigInt]) : BigInt = b match {
                case head :: tl => {
                    val c = a*head 
                    tl match {
                        case Nil => if((c*c)<BigInt(n)) c else a
                        case _   => if((c*c)<BigInt(n)) check(c,tl) else check(a,tl.tail)
                    }
                }  
                case Nil => a
            }
            check(np.head,np.tail).toInt
        }
    }

    def gen[T<:Method](N:Int) : ffter[T] = {
        val i = if(Prime.isPrime(N)) N else choose_factor(N)
        val o = N/i
        def pp(n: Int) = {
            if(n==2 || !wMethod) pb[T](n,false) else {
                val first   = gen[T](n-1).asInstanceOf[ffter[Method]]
                val second  = rgen[T](n-1).asInstanceOf[ffter[Method]]
                wpbp[T](n,first, second, false)
            }
        }
        if(i==1) pp(N)
        else if (o==1) pp(N)
        else {
            val ib = gen[T](i).asInstanceOf[ffter[Method]]
            val ob = gen[T](o).asInstanceOf[ffter[Method]]
            if(Prime.gcd(i,o)==1) psb(ib,ob,false) 
            else msb(ib,ob,false)
        }
    }
    def rgen[T<:Method](N:Int) : ffter[T] = {
        val o = if(Prime.isPrime(N)) N else choose_factor(N)
        val i = N/o
        def pp(n: Int) = {
            if(n==2 || !wMethod) pb[T](n,true) else {
                // same as gen
                val first   = gen[T](n-1).asInstanceOf[ffter[Method]] 
                val second  = rgen[T](n-1).asInstanceOf[ffter[Method]]
                wpbp[T](n,first, second,true)
            }
        }
        if(i==1) pp(N)
        else if (o==1) pp(N)
        else {
            val ib = rgen[T](i).asInstanceOf[ffter[Method]]
            val ob = rgen[T](o).asInstanceOf[ffter[Method]]
            if(Prime.gcd(i,o)==1) psb(ib,ob,true) 
            else msb(ib,ob,true)
        }
    }
}

