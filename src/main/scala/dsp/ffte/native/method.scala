package ffte.native
import breeze.math.{Complex}


import ffte.algorithm.FFTGen.{Method,genericFT,DFT,psbFFT,msbFFT,wpbFFT}
import ffte.algorithm.{circularOrder,FFTMisc}

trait NativeFFT {
    val S  :Int
    def transform(x:Array[Complex]) : Array[Complex]
}

class Native(shift:Int) extends Method {
    def apply(entity: genericFT, prefix: String):NativeFFT = {
        entity match {
            case d : DFT            => new ndft(d.N,shift)
            case d : psbFFT[Native] => new psb(shift,d)
            case d : msbFFT[Native] => new msb(shift,d)
            case d : wpbFFT[Native] => new wpb(shift,d)
            case _                  => new ndft(2,shift)
        }
    }
}

sealed class ndft(N:Int,val S:Int) extends NativeFFT {
    def dft(x:Array[Complex]) = {
		val n = x.length
		(0 until n).map{i => x.zipWithIndex.map{ case (a,k) => a*FFTMisc.wnk(i*k,n)}.sum}.toArray
	}
    def transform(x:Array[Complex]) = {
        assert(x.length==N,s"Wrong number of data")
        dft(x).map(_/(1<<S))
    }
}

sealed class psb[T<:Method](val S:Int,val p:psbFFT[T]) extends NativeFFT {
    val first  = p.ib
    val second = p.ob
    val first_shift = Math.round(S*Math.log(first.N)/Math.log(second.N*first.N)).toInt
    val second_shift = S - first_shift
    val fg = new Native(first_shift)
    val sg = new Native(second_shift)
    val f_block = fg(first,"f")
    val s_block = sg(second,"s")
    def transform(x:Array[Complex]): Array[Complex] = {
        val r = new Array[Complex](x.length)
        for(i <- 0 until second.N) {
            val fout = f_block.transform(x.slice(i*first.N,(i+1)*first.N))
            for(k <- 0 until first.N) {
                r(k*second.N+i) = fout(k)
            }
        }
        for(i <- 0 until first.N) {
            val fout = s_block.transform(r.slice(i*second.N,(i+1)*second.N))
            for(k <- 0 until second.N) {
                r(i*second.N+k) = fout(k)
            }
        }
        r
    }
}

sealed class msb[T<:Method](val S:Int,val p:msbFFT[T]) extends NativeFFT {
    val first  = p.ib
    val second = p.ob
    val first_shift = Math.round(S*Math.log(first.N)/Math.log(second.N*first.N)).toInt
    val second_shift = S - first_shift
    val fg = new Native(first_shift)
    val sg = new Native(second_shift)
    val f_block = fg(first,"f")
    val s_block = sg(second,"s")
    val rotator = (for(i <- 0 until second.N;k <- 0 until first.N) yield FFTMisc.wnk(p.p.first.store_tab(k)*p.p.second.load_tab(i),p.N)).toArray
    def transform(x:Array[Complex]): Array[Complex] = {
        val r = new Array[Complex](x.length)
        for(i <- 0 until second.N) {
            val fout = f_block.transform(x.slice(i*first.N,(i+1)*first.N))
            for(k <- 0 until first.N) {
                r(k*second.N+i) = fout(k)*rotator(k+i*first.N)
            }
        }
        for(i <- 0 until first.N) {
            val fout = s_block.transform(r.slice(i*second.N,(i+1)*second.N))
            for(k <- 0 until second.N) {
                r(i*second.N+k) = fout(k)
            }
        }
        r
    }
}

sealed class wpb(val S:Int,p:wpbFFT[Native]) extends NativeFFT {
    val oc = circularOrder(p.N)
    val wf = oc.wcoeff

    val first_shift  = S/2
    val second_shift = S-first_shift
    val fg = new Native(first_shift)
    val sg = new Native(second_shift)

    val f_block = fg(p.ib,"f")
    val s_block = sg(p.ob,"s")
    val coeff = (for(i <- 0 until p.N-1) yield wf(p.p.first.store_tab(i))).toArray
    def transform(x: Array[Complex]): Array[Complex] = {
        val sum = (Complex(0.0,0.0) /: x)(_+_) /(1<<S)
        val x0  = x(0) / (1<<S)
        val tf  = f_block.transform(x.slice(1,x.length))
        val wtf = tf.zipWithIndex.map{ case(x,i) => x*coeff(i)}
        val ts  = s_block.transform(wtf)
        val r   = new Array[Complex](x.length)
        r(0) = sum
        ts.zipWithIndex.foreach{ case(x,i) => r(i+1) = x+x0}
        r
    }
}