package ffte.native
import breeze.math.Complex
import ffte.algorithm.FFTGen.ffter
class NFFT(p:ffter[Native],val S:Int) extends NativeFFT {
    val inBuf  = new Array[Complex](p.rN)
    val g      = new Native(S)
    val fft    = g(p.fft,"top") 
    def transform(x: Array[Complex]): Array[Complex] = {
        for(i <- 0 until p.rN) {
            inBuf(i) = x(p.load_tab(i))
        }
        val tf = fft.transform(inBuf)
        val r  = new Array[Complex](p.rN)
        for(i <- 0 until p.rN) {
            r(p.store_tab(i)) = tf(i)
        }
        r
    }
}
class NIFFT(p:ffter[Native],val S:Int) extends NativeFFT {
    val inBuf  = new Array[Complex](p.rN)
    val g      = new Native(S)
    val fft    = g(p.fft,"top") 
    def swap(x:Complex) : Complex = {
        Complex(x.im,x.re)
    }
    def transform(x: Array[Complex]): Array[Complex] = {
        for(i <- 0 until p.rN) {
            inBuf(i) = swap(x(p.load_tab(i)))
        }
        val tf = fft.transform(inBuf)
        val r  = new Array[Complex](p.rN)
        for(i <- 0 until p.rN) {
            r(p.store_tab(i)) = swap(tf(i))
        }
        r
    }
}