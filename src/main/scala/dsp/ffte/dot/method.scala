package ffte.dot

import ffte.algorithm.FFTGen.{Method,genericFT,msbFFT,psbFFT,wpbFFT,pb,fft1,ffter}
class Dot extends Method {
    private def edge(n0:String,n1:String,g:ffter[Method]) = g match {
        case gp:pb[Method] => s"$n0 -> $n1"
        case _    => s"${n0}_s -> ${n1}_f [lhead=cluster_$n0 ltail=cluster_$n1];"
    }
        
    private def node(name: String,entity: genericFT) = 
        s"""label="$name: $entity" """
    def apply(entity: genericFT, prefix: String): Any = {
        entity match {
            case msbFFT(p) => 
                s"subgraph cluster_${prefix}{\n" +
                    s"${node(prefix,entity)};\n" +
                    this.apply(p.first.fft,s"${prefix}_f") +
                    this.apply(p.second.fft,s"${prefix}_s") + "\n" +
                    s"${edge(s"${prefix}_f",s"${prefix}_s",p.first)}" + "\n" +
                "}" + "\n" 
            case psbFFT(p) => 
                s"subgraph cluster_${prefix}{\n" +
                    s"${node(prefix,entity)};\n" +
                    this.apply(p.first.fft,s"${prefix}_f") +
                    this.apply(p.second.fft,s"${prefix}_s") + "\n" +
                    s"${edge(s"${prefix}_f",s"${prefix}_s",p.first)}" + "\n" +
                "}" + "\n"
            case wpbFFT(_,p) => 
                s"subgraph cluster_${prefix}{\n" +
                    s"${node(prefix,entity)};\n" +
                    this.apply(p.first.fft,s"${prefix}_f") +
                    this.apply(p.second.fft,s"${prefix}_s") + "\n" +
                    s"${edge(s"${prefix}_f",s"${prefix}_s",p.first)}" + "\n" +
                "}" + "\n"
            case _ => s"""$prefix[label="dft2"];\n""" 
        }
    }
    def graph(entity: genericFT) : String = {
        "digraph G {" + "\n" +
        "compound=true;" + "\n" +
        this.apply(entity,"top") + "\n" +
        "}"
    }
}