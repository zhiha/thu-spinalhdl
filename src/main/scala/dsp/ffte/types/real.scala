package ffte.types

import spinal.core._
import ffte.property.getResolution

case class FixReal(val width:Int,val resolution:Int=getResolution()) extends Bundle {
    val d = SFix((width-1+resolution) exp, resolution exp)
    def iOne = (1<<(-resolution-1))
    def iMax = (1<<width)-1
    def resize(dW:Int) = {
        val r = FixReal(dW,resolution)
        r.d.raw := this.d.raw.resize(dW)
        r
    }
    def expand = this.resize(width+1)
    def + (right: FixReal): FixReal = {
        val ret = FixReal(width,resolution)
        ret.d := (this.d + right.d).truncated
        ret
    }
    def - (right: FixReal): FixReal = {
        val ret = FixReal(width,resolution)
        ret.d := this.d - right.d
        ret
    }
    def * (right: FixReal): FixReal = {
        val ret = FixReal(this.width+right.width,this.resolution+right.resolution)
        ret.d.raw := this.d.raw*right.d.raw 
        ret
    }
    def unary_- : FixReal = {
        val ret = FixReal(width,resolution)
        ret.d.raw := -this.d.raw.symmetry
        ret
    }
    def j(that: FixReal) : FixComplex = {
        val ret = FixComplex(width,resolution)
        ret.re := this
        ret.im := that
        ret
    }
    def :=(that: SFix): Unit     = {
        this.d := that
    }
    def abs : UInt = {
        this.d.raw.absWithSym 
    }
    def zero = {
        this.d.raw := S(0)
        this
    }
    def fromInt(x:Int) = {
        this.d.raw := S(x)
        this
    }
    def fromDouble(x:Double) = {
        val d = Math.round(x*iOne).toInt
        val satd =  if(d>iMax) iMax 
                    else if (d < -iMax) -iMax 
                    else d
        this.d.raw := S(satd)
    }
    def sat(n:Int) = {
        val ret = FixReal(width,resolution)
        ret.d.raw := this.d.raw - (this.d.raw|>>n)
        ret
    }
    def fixTo(w:Int,s:Int) = {
        val ret = FixReal(w,resolution)
        ret.d.raw := this.d.raw.fixTo(w+s-1 downto s, RoundType.ROUNDTOINF,sym=true)
        ret
    }
    def fixTo(w:Int,s:Int,r:Int) = {
        val ret = FixReal(w,r)
        ret.d.raw := this.d.raw.fixTo(w+s-1 downto s, RoundType.ROUNDTOINF,sym=true)
        ret
    }
    def cut(w:Int,s:Int) = {
        val ret = FixReal(w,resolution)
        ret.d.raw := this.d.raw(w+s-1 downto s)
        ret
    }
    def cut(w:Int,s:Int,r:Int) = {
        val ret = FixReal(w,r)
        ret.d.raw := this.d.raw(w+s-1 downto s)
        ret
    }
    def cos(m:Int,N:Int) = {
        val ret = FixReal(width,resolution)
        val cos = Math.round(Math.cos(m*2*Math.PI/N)*iOne).toInt
        ret.d.raw := cos
        ret
    }
    def sin(m:Int,N:Int) = {
        val ret = FixReal(width,resolution)
        val sin = Math.round(Math.sin(m*2*Math.PI/N)*iOne).toInt
        ret.d.raw := sin
        ret
    }
    def one = {
        val ret = FixReal(width,resolution)
        ret.d.raw := iOne
        ret
    }
}
