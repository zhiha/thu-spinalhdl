package ffte.types

import breeze.math.Complex
import spinal.core._
import spinal.lib._

import ffte.property.{getResolution,getComplexMulMethod}
object FixComplex {
    def fromVec(x:Vec[FixReal]) : FixComplex = {
        assert(x.length==2,s"only vec 2 real can convert to complex")
        val r = FixComplex(x(0).width,x(0).resolution)
        r.re := x(0)
        r.im := x(1)
        r
    }
    def sum(a:Vec[FixComplex],sz:Int,shift:Int,d:Int,ce:Bool):FixComplex = {
        val sW  = log2Up(sz)
        val dW  = a(0).width
        val res = a(0).resolution
        if(d==1) {
            val r = RegInit(FixComplex(dW,res).zero)
            val suma = new Area {
                val b = a.map(_.resize(dW+sW))
                when(ce) {
                    if(sz==1) {
                        r := b(0).cut(dW,shift)
                    } else {
                        r := b.reduce(_+_).fixTo(dW,shift)
                    }
                }
            }
            r
        }
        else if(d>sW) Delay(sum(a,sz,shift,sW,ce),d-sW,ce) 
        else {
            val r = RegInit(FixComplex(dW,res).zero)
            val suma = new Area {
                when(ce) {
                    if(sz==1) {
                        r := Delay(a(0).resize(dW+sW).cut(dW,shift),d-1,ce)
                    } else {
                        val half = sz/2
                        val other = sz-half
                        val s = if(shift>=1) 1 else 0
                        r := (sum(Vec((0 until half).map(a(_))),half,shift-s,d-1,ce) + sum(Vec((half until sz).map(a(_))),other,shift-s,d-1,ce)).fixTo(dW,s)
                    }
                }
            }
            r
        }
    }
    def sumc(a:Vec[FixComplex],sz:Int,shift:Int,d:Int,ce:Bool):FixComplex = {
        val sW = log2Up(sz)
        val dW = a(0).width
        val res = a(0).resolution
        if(d==1) {
            val r = RegInit(FixComplex(dW,res).zero)
            val suma = new Area {
                val b = a.map(_.resize(dW+sW))
                when(ce) {
                    if(sz==1) {
                        r := b(0).cut(dW,shift)
                    } else {
                        r := b.reduce(_+_).cut(dW,shift)
                    }
                }
            }
            r
        }
        else if(d>sW) Delay(sum(a,sz,shift,sW,ce),d-sW,ce) 
        else {
            val r = RegInit(FixComplex(dW,res).zero)
            val suma = new Area {
                when(ce) {
                    if(sz==1) {
                        r := Delay(a(0).resize(dW+sW).cut(dW,shift),d-1,ce)
                    } else {
                        val half = sz/2
                        val other = sz-half
                        val s = if(shift>=1) 1 else 0
                        r := (sum(Vec((0 until half).map(a(_))),half,shift-s,d-1,ce) + sum(Vec((half until sz).map(a(_))),other,shift-s,d-1,ce)).cut(dW,s)
                    }
                }
            }
            r
        }
    }
}

case class FixComplex(val width:Int,val resolution:Int=getResolution()) extends Bundle {
    val re = FixReal(width,resolution)
    val im = FixReal(width,resolution)
    def expand = {
        val ret = FixComplex(width+1,resolution)
        ret.re := this.re.expand
        ret.im := this.im.expand
        ret
    }
    def + (right: FixComplex): FixComplex = {
        val ret = FixComplex(width,resolution)
        ret.re := this.re + right.re
        ret.im := this.im + right.im
        ret
    }
    
    def - (right: FixComplex): FixComplex = {
        val ret = FixComplex(width,resolution)
        ret.re := this.re - right.re
        ret.im := this.im - right.im
        ret
    }
    
    def :=(that: FixComplex): Unit     = {
        this.re := that.re
        this.im := that.im
    }

    def * (that: FixComplex): FixComplex = {
        val ret = FixComplex(this.width+that.width,this.resolution+that.resolution)
        if(getComplexMulMethod()) {
            ret.re.d.raw := this.re.d.raw*that.re.d.raw - this.im.d.raw*that.im.d.raw
            ret.im.d.raw := this.im.d.raw*that.re.d.raw + this.re.d.raw*that.im.d.raw
        } else{
            val this_add = this.re.resize(width+1) + this.im.resize(width+1)
            val that_add = that.re.resize(width+1) + that.im.resize(width+1)
            val re_re    = this.re * that.re
            val im_im    = this.im * that.im
            val add_add  = this_add * that_add
            ret.re := re_re - im_im
            ret.im := (add_add - re_re - im_im).cut(this.width+that.width,0)
        }
        ret
    }

    def * (that: FixReal): FixComplex = {
        val ret = FixComplex(this.width+that.width)
        ret.re.d.raw := this.re.d.raw*that.d.raw
        ret.im.d.raw := this.im.d.raw*that.d.raw
        ret
    }
    def unary_- : FixComplex = {
        val ret = FixComplex(width,resolution)
        ret.re := -this.re
        ret.im := -this.im
        ret
    }
    def j = {
        val ret = FixComplex(width,resolution)
        ret.re := -this.im
        ret.im := this.re
        ret
    }
    def conj = {
        val ret = FixComplex(width,resolution)
        ret.re := this.re
        ret.im := -this.im
        ret
    }
    def cabs : UInt = {
        val rabs = this.re.abs
        val iabs = this.im.abs
        (rabs>iabs) ? (rabs+(iabs |>> 1)) | ((rabs |>> 1)+iabs)
    }
    def fixTo(w:Int,s:Int) = {
        val ret = FixComplex(w,resolution)
        ret.re := this.re.fixTo(w,s)
        ret.im := this.im.fixTo(w,s)
        ret
    }
    def fixTo(w:Int,s:Int,r:Int) = {
        val ret = FixComplex(w,r)
        ret.re := this.re.fixTo(w,s,r)
        ret.im := this.im.fixTo(w,s,r)
        ret
    }
    def cut(w:Int,s:Int) = {
        val ret = FixComplex(w,resolution)
        ret.re := this.re.cut(w,s)
        ret.im := this.im.cut(w,s)
        ret
    }
    def cut(w:Int,s:Int,r:Int) = {
        val ret = FixComplex(w,r)
        ret.re := this.re.cut(w,s,r)
        ret.im := this.im.cut(w,s,r)
        ret
    }
    def one = {
        val ret = FixComplex(width,resolution)
        ret.re := this.re.one
        ret.im := this.im.zero
        ret
    }
    def zero = {
        val ret = FixComplex(width,resolution)
        ret.re.d.raw := S(0)
        ret.im.d.raw := S(0)
        ret
    }
    def resize(dW:Int) = {
        val r = FixComplex(dW,resolution)
        r.re := this.re.resize(dW)
        r.im := this.im.resize(dW)
        r
    }
    def sat(m:Int) = {
        val r = FixComplex(width)
        r.re := this.re.sat(m)
        r.im := this.im.sat(m)
        r
    }
    def expj(m:Int,N:Int) = {
        val r = FixComplex(width,resolution) 
        r.re := this.re.cos(m,N)
        r.im := this.re.sin(m,N)
        r
    }
    def swap = {
        val r = FixComplex(width,resolution)
        r.re := this.im
        r.im := this.re
        r
    }
    def toVec = Vec(this.re,this.im)
    def fromComplex(x:Complex) = {
        this.re.fromDouble(x.re)
        this.im.fromDouble(x.im)
        this
    }
}

