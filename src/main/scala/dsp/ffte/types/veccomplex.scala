package ffte.types

import breeze.math.Complex
import spinal.core._
import spinal.lib._

import ffte.property.{getResolution}

case class VecComplex(val size:Int, val width:Int, val resolution:Int=getResolution()) extends Bundle {
    val d = Vec(FixComplex(width,resolution),size)
    def dup = VecComplex(size,width,resolution)
    def expand = {
        val r = VecComplex(size,width+1,resolution)
        for (i <- 0 until size) r.d(i) := this.d(i).expand
        r
    }
    def + (that: VecComplex): VecComplex = {
        val r = this.dup
        for (i <- 0 until size) r.d(i) := this.d(i) + that.d(i)
        r
    }    
    def - (that: VecComplex): VecComplex = {
        val r = this.dup
        for (i <- 0 until size) r.d(i) := this.d(i) - that.d(i)
        r
    }    
    def :=(that: VecComplex): Unit     = {
        for (i <- 0 until size) d(i) := that.d(i)
    }
    def * (that: VecComplex): VecComplex = {
        assert(size == that.size,s"mul vec complex must be same size.")
        val r = VecComplex(this.size,this.width+that.width,this.resolution+that.resolution)
        for (i <- 0 until size) r.d(i) := this.d(i) * that.d(i)
        r
    }
    def * (that: FixReal): VecComplex = {
        val r = VecComplex(this.size,this.width+that.width,this.resolution+that.resolution)
        for (i <- 0 until size) r.d(i) := this.d(i) * that
        r
    }
    def * (that: FixComplex): VecComplex = {
        val r = VecComplex(this.size,this.width+that.width,this.resolution+that.resolution)
        for (i <- 0 until size) r.d(i) := this.d(i) * that
        r
    }
    def unary_- : VecComplex = {
        val r = this.dup
        for (i <- 0 until size) r.d(i) := -this.d(i)
        r
    }
    def j = {
        val r = this.dup
        for (i <- 0 until size) r.d(i) := this.d(i).j
        r
    }
    def conj = {
        val r = this.dup
        for (i <- 0 until size) r.d(i) := this.d(i).conj
        r
    }    
    def cut(w:Int,s:Int) = {
        val r = VecComplex(size,w,resolution)
        for (i <- 0 until size) r.d(i) := this.d(i).cut(w,s)
        r
    }
    def cut(w:Int,s:Int,r:Int) = {
        val ret = VecComplex(size,w,r)
        for (i <- 0 until size) ret.d(i) := this.d(i).cut(w,s,r)
        ret
    }
    def one = {
        val r = this.dup
        for (i <- 0 until size) r.d(i) := this.d(i).one
        r
    }
    def sat(m:Int) = {
        val r = this.dup
        for (i <- 0 until size) r.d(i) := this.d(i).sat(m)
        r
    }
    def expj(m:Int,N:Int) = {
        val r = this.dup
        for (i <- 0 until size) r.d(i) := this.d(i).expj(m,N)
        r    
    }
    def fromComplex(x:Complex) = {
        for (i <- 0 until size) this.d(i).fromComplex(x)
        this
    }
    def fixTo(w:Int,s:Int) = {
        val ret = VecComplex(size,w,resolution)
        for (i <- 0 until size) {
            ret.d(i) := this.d(i).fixTo(w,s)
        }
        ret
    }
    def fixTo(w:Int,s:Int,r:Int) = {
        val ret = VecComplex(size,w,r)
        for (i <- 0 until size) {
            ret.d(i) := this.d(i).fixTo(w,s,r)
        }
        ret
    }
    def subdivideIn(ss:Int) = {
        val ret = Vec(VecComplex(ss,width,resolution),size/ss)
        for (i <- 0 until size) {
            val sss = size/ss
            val n = i%sss
            val k = i/sss
            ret(n).d(k) := d(i) 
        }
        ret
    }
    def sW = log2Up(size)
    def reverse_tab = {
        (for (i <- 0 until size) yield {
            var x = 0
            for(j <- 0 until sW) {
                x |= ((i>>j)&1)<<(sW-j-1)
            }
            x
        }).toArray
    }
    def reverse_order_bits = {
        assert(size == (1<<sW),"reverse order must be 2^n")
        val r = this.dup
        for (i <- 0 until size) r.d(i) := this.d(reverse_tab(i))
        r
    }
    def rotate(n:Int) = {
        val r = this.dup
        for(i<-n until size) r.d(i-n)      := this.d(i)
        for(i<- 0 until n)   r.d(size+i-n) := this.d(i)
        r
    }
    def swap = {
        val r = this.dup
        for(i<-0 until size) r.d(i)      := this.d(i).swap
        r
    }
    def resize(dW:Int) = {
        val r = new VecComplex(size,dW,resolution)
        for(i<-0 until size) r.d(i)      := this.d(i).resize(dW)
        r
    }
    def zero = {
        val r = this.dup
        for(i<-0 until size) r.d(i)      := this.d(i).zero
        r
    }
}