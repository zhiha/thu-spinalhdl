package ffte.misc

import spinal.core._
import spinal.lib._

case class delay(d:Int,N:Int) {
    def delayPulse(s:Bool,fire:Bool) = {
        if (d<16) {
            Delay(s,d,fire,False)
        } else 
        {
            val da = new Area {
                val dW = log2Up(d)+2
                val delayFifo = new syncfifo(UInt(dW bits),(1<<dW)/N+8)
                val active = RegInit(False)
                val cnt    = RegInit(U(0,dW bits))
                delayFifo.io.push.valid := fire & s
                delayFifo.io.push.payload := cnt+d
                delayFifo.io.flush := False
                when(fire) {
                    cnt    := cnt+1
                }
                val hit = fire & (cnt===delayFifo.io.pop.payload) & delayFifo.io.pop.valid
                delayFifo.io.pop.ready := hit
            }
            da.hit
        }
    }
}