package ffte.misc

import spinal.core._
import spinal.lib._

class syncfifo[T <: Data](itype:HardType[T],size:Int) extends Component {
    val io = new Bundle {
        val push = slave(Stream(itype()))
        val pop  = master(Stream(itype()))
        val flush = in Bool()
    }
    
    val sW       = log2Up(size)
    val ram      = Mem(itype, size)
    val pushPtr  = RegInit(U(0,sW bits))
    val popPtr   = RegInit(U(0,sW bits))
    val pushNext = UInt(sW bits)
    val pushInc  = UInt(sW bits)
    val popNext  = UInt(sW bits)
    val pushing  = io.push.fire
    val poping   = io.pop.fire
    val empty    = pushPtr === popPtr
    val full     = RegNext(pushInc === popPtr,False)
    pushPtr     := pushNext
    popPtr      := popNext
    io.push.ready  := !full
    io.pop.valid   := !empty 
    io.pop.payload := ram.readSync(
        address = popNext
    )
    ram.write(
        address = pushPtr, 
        data    = io.push.payload, 
        enable  = pushing
    )
    pushNext := pushPtr
    popNext  := popPtr
    when(io.flush){
        pushNext := U(0)
        popNext  := U(0)
    }.otherwise {
        when(pushing) {
            when(pushPtr===size-1) {
                pushNext := U(0)
            }.otherwise {
                pushNext := pushPtr+1
            }
        }
        when(poping) {
            when(popPtr===size-1) {
                popNext := U(0)
            }.otherwise {
                popNext := popPtr+1
            }
        }
    }
    pushInc := Mux(pushPtr===size-1, U(0), (pushPtr+1))
}