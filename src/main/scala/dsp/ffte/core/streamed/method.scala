package ffte.core.streamed

import ffte.algorithm.FFTGen.{Method,genericFT,DFT,psbFFT,msbFFT,wpbFFT}
import spinal.core._

class Streamed(shift:Int) extends Method {
    def apply(entity: genericFT, prefix: String): streamedComponents = {
        entity match {
            case d : DFT                 => new sdft2(shift)
            case d : psbFFT[Streamed]    => new psb(shift,d)
            case d : msbFFT[Streamed]    => new msb(shift,d)
            case d : wpbFFT[Streamed]    => new wpb(shift,d)
            case _                       => new sdft2(shift)
        }
    }
}

