package ffte.core.streamed

import spinal.core._
import spinal.lib._

import ffte.algorithm.FFTGen.{msbFFT}
import ffte.property.FFTIOConfig
class msb[T<:Streamed](val S:Int,val p:msbFFT[T]) extends combineStreamedComponents[T] {
    val s_block = FFTIOConfig(interW,oW) on new Area {
        val u = sg(second,"s") 
        val delay = u.delay
    }

    val r = rotator(interW,p.p.first.store_tab,p.p.second.load_tab)
    val interleave = SInterleaver(interW,first.N,second.N)

    f_block.u.io.q   >> r.io.d
    r.io.q           >> interleave.io.d
    interleave.io.q  >> s_block.u.io.d
    
    io.d >> f_block.u.io.d
    io.q << s_block.u.io.q
    
    def delay = f_block.delay + interleave.delay + s_block.delay + r.delay
}