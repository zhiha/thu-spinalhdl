package ffte.core.streamed

import spinal.core._
import spinal.lib._

import ffte.types.{VecComplex,FixComplex}
import ffte.core.samewidthed
import ffte.property.{getTwiddleResolution}

object rotator {
    def apply(dW:Int,first:Array[Int],second:Array[Int]) : streamedComponents = 
        if(first.length==2 && second.length==2) (new rotator4(dW)) else (new rotator(dW,first,second))
}

sealed class rotator4(val dW:Int) extends streamedComponents with samewidthed {
    val io = iogen
    io.d.ready := io.q.ready
    val fire = io.d.fire
    io.q.valid := fire
    val q = RegInit(VecComplex(G,dW).zero)
    
    val cnt = Counter(4,inc=fire)
    
    when(fire) {
        when(io.d.payload.last) {
            cnt.clear
        }
        q := cnt.value.mux( 
            0 -> (io.d.payload.fragment),
            1 -> (io.d.payload.fragment),
            2 -> (io.d.payload.fragment),
            3 -> (-io.d.payload.fragment.j)
        )
    }
    
    io.q.payload.fragment := q
    def delay = 1
    io.q.payload.last := RegNextWhen(io.d.payload.last,fire)
}

sealed class rotator(val dW:Int,first:Array[Int],second:Array[Int]) extends streamedComponents with samewidthed {
    val io = iogen
    io.d.ready := io.q.ready
    val fire = io.d.fire
    io.q.valid := fire
    val tW = 1-getTwiddleResolution()
    val tr = getTwiddleResolution()
    val N = first.size * second.size
    val d = RegNextWhen(io.d.payload.fragment,fire)
    val cnt = Counter(N,inc=fire)
    val mem = Mem(FixComplex(tW,tr)
        ,initialContent = (for(s <- 0 until second.size; f <- 0 until first.size) 
        yield FixComplex(tW,tr).expj(-second(s)*first(f),N))
    )
    val f = mem.readSync(
        address = cnt,
        enable  = fire
    )
    val q = RegInit(VecComplex(G,dW+tW,d.resolution+tr).zero)
    when(fire) {
        when(io.d.payload.last) {
            cnt.clear
        }
        q := d*f
    }
    io.q.payload.fragment := q.cut(dW,-tr-1,d.resolution)
    def delay = 2
    io.q.payload.last := Delay(io.d.payload.last,delay,fire)
}