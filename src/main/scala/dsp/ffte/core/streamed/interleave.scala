package ffte.core.streamed

import spinal.core._
import spinal.lib._

import ffte.types.VecComplex
import ffte.misc
import ffte.core.{samewidthed}

object SInterleaver {
    def apply(dW:Int,first:Int,second:Int) : streamedComponents = 
        if(first==second) (new eInterleaver(dW,first))
        else if(first<second && (second%first)==0 ) (new pInterleaver(dW,first,second))
        else (new pInterleaver(dW,first,second)) 
    def hold[T <: Data](x:Stream[T]) : T = {
        val ha = new Area {
            val r = Reg(x.payloadType())
            r := x.payload
        }
        (x.valid) ? x.payload | ha.r
    }
}

sealed class pInterleaver(val dW:Int,first:Int,second:Int) extends streamedComponents with samewidthed {
    val io = iogen
    val fAW = log2Up(first)
    val sAW = log2Up(second)

    val mem = Mem(VecComplex(G,dW),1<<(fAW+sAW+1))
    
    io.d.ready := io.q.ready
    io.q.valid := io.d.fire
    val fire = io.d.fire

    
    def delay = first*second + 1
    
    val start = misc.delay(delay-1,first*second).delayPulse(io.d.payload.last,fire)
    
    val rfCnt = Counter(first,inc=fire)
    val rsCnt = Counter(second,inc=rfCnt.willOverflow)
    val rpp   = Counter(2,inc=rsCnt.willOverflow)
    val wsCnt = Counter(second,inc=fire)
    val wfCnt = Counter(first,inc=wsCnt.willOverflow)
    val wpp   = RegInit(U(0, 1 bits))
    
    when(fire) {
        when(io.d.payload.last) {
            rfCnt.clear
            rsCnt.clear
        }
        when(rsCnt.willOverflowIfInc && rfCnt.willOverflowIfInc) {
            wfCnt.clear
            wsCnt.clear
            wpp := rpp.value
        }
    }
    mem.write(
        address = rpp @@ rsCnt @@ rfCnt,
        data    = io.d.payload.fragment,
        enable  = fire
    )
    io.q.payload.fragment := mem.readSync(
        address = wpp @@ wsCnt @@ wfCnt,
        enable  = fire
    )    
    io.q.payload.last := RegNextWhen(start,fire)

}

sealed class eInterleaver(val dW:Int,N:Int) extends streamedComponents with samewidthed {
    val io = iogen
    io.d.ready := io.q.ready
    io.q.valid := io.d.fire
    val fire = io.d.fire

    val AW = log2Up(N)
    
    val mem = Mem(VecComplex(G,dW),1<<(2*AW))
    
    def delay = N*N+1
    val start = misc.delay(delay-1,N).delayPulse(io.d.payload.last,fire)
    
    val rfCnt = Counter(N,inc=fire)
    val rsCnt = Counter(N,inc=rfCnt.willOverflow)
    val rpp   = Counter(2,inc=rsCnt.willOverflow)

    val wsCnt = Counter(N,inc=fire)
    val wfCnt = Counter(N,inc=wsCnt.willOverflow)
    val wpp   = RegInit(U(0, 1 bits))
    when(fire) {
        when(io.d.payload.last) {
            rfCnt.clear
            rsCnt.clear
        }
        when(rsCnt.willOverflowIfInc && rfCnt.willOverflowIfInc) {
            wfCnt.clear
            wsCnt.clear
            wpp := rpp.value
        }
    }
    mem.write(
        address = ((rpp===0)? (rsCnt @@ rfCnt) | (rfCnt @@ rsCnt)),
        data    = io.d.payload.fragment,
        enable  = fire
    )
    io.q.payload.fragment := mem.readSync(
        address = ((wpp===0)? (wsCnt @@ wfCnt) | (wfCnt @@ wsCnt)),
        enable  = fire
    )    
    io.q.payload.last := RegNextWhen(start,fire)
}