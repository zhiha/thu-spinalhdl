package ffte.core.streamed

import spinal.core._
import spinal.lib._

import ffte.types.{VecComplex}

import ffte.property.{getFFTIW,getFFTOW,FFTIOConfig}
import ffte.algorithm.FFTGen.{Method,combineFT}
import ffte.core.{fft,combine,streamedIO,getStreamedGroup}

trait streamedComponents extends Component with fft {
    val G  = getStreamedGroup()
    val io : streamedIO
    def iogen = streamedIO(G,dW,oW)
}

trait combineStreamedComponents[T<:Method] extends streamedComponents with combine[T] {
    val fg = new Streamed(first_shift)
    val sg = new Streamed(second_shift)
    val io = iogen
    val f_block = FFTIOConfig(dW,interW) on new Area {
        val u = fg(first,"f") 
        val delay = u.delay
    }
}

object streamedComponents {
    def inv(x : Stream[Fragment[VecComplex]]) = {
        val np = Fragment(x.dup)
        np.last     := x.payload.last
        np.fragment := x.payload.fragment.swap
        x.translateWith(np)
    }
    def resize(x : Stream[Fragment[VecComplex]],dW:Int)= {
        val np = Fragment(x.dup)
        np.last     := x.payload.last
        np.fragment := x.payload.fragment.resize(dW)
        x.translateWith(np)
    }
}


