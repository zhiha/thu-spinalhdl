package ffte.core.streamed

import spinal.core._
import spinal.lib._

import ffte.algorithm.FFTGen.{psbFFT}
import ffte.property.{FFTIOConfig}

class psb[T<:Streamed](val S:Int, val p:psbFFT[T]) extends combineStreamedComponents[T] {
    val s_block = FFTIOConfig(interW,oW) on new Area {
        val u = sg(second,"s") 
        val delay = u.delay
    }
    val interleave = SInterleaver(interW,first.N,second.N)
    f_block.u.io.q >> interleave.io.d
    interleave.io.q >> s_block.u.io.d
    io.d >> f_block.u.io.d
    io.q << s_block.u.io.q
    def delay = f_block.delay + interleave.delay + s_block.delay 
}