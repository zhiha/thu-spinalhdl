package ffte.core.streamed

import spinal.core._
import spinal.lib._

import ffte.types.{FixComplex,FixReal,VecComplex}
import ffte.misc
import ffte.core.{widthed,StreamedBus}
class sdft2(val S:Int) extends streamedComponents with widthed {
    val io = iogen
    val c2ri = new C2RI(G,dW)
    val ri2  = new ridft2(G,S,dW,oW)
    val ri2c = new RI2C(G,oW)
    c2ri.io.q >> ri2.io.d
    ri2.io.q  >> ri2c.io.d
    io.d >> c2ri.io.d
    io.q << ri2c.io.q
    def delay = c2ri.delay + ri2.delay + ri2c.delay 
} 

class ridft2(val G:Int, shift:Int,dW:Int,oW:Int) extends Area {
    val io = new Area {
        val d = StreamedBus(Vec(Vec(FixReal(dW),2),G))
        val q = StreamedBus(Vec(Vec(FixReal(oW),2),G))
    }
    val q = RegInit(Vec(Vec(FixReal(dW+1).zero,2),G))
    io.d.ready := io.q.ready
    io.q.valid := io.d.fire
    when(io.d.fire) {
        for(i <- 0 until G) {
            q(i)(0) := io.d.payload.fragment(i)(0).resize(dW+1) + io.d.payload.fragment(i)(1)
            q(i)(1) := io.d.payload.fragment(i)(0).resize(dW+1) - io.d.payload.fragment(i)(1)
        }
    }
    for(i <- 0 until G) {
        io.q.payload.fragment(i)(0) := q(i)(0).fixTo(oW,shift)
        io.q.payload.fragment(i)(1) := q(i)(1).fixTo(oW,shift)
    }

    io.q.payload.last   := misc.delay(delay,2).delayPulse(io.d.payload.last,io.d.fire)
    def delay = 1
}

class C2RI(G:Int,dW:Int) extends Area {
    val io = new Area {
        val d = StreamedBus(VecComplex(G,dW))
        val q = StreamedBus(Vec(Vec(FixReal(dW),2),G))
    }
    io.d.ready := io.q.ready
    io.q.valid   := io.d.fire
    val d = Vec(Vec(FixReal(dW),2),G)
    val q = Vec(Vec(FixReal(dW),2),G)
    val c = RegInit(False)
    
    when(io.d.fire) {
        when(io.d.payload.last) {
            c := False
        }.otherwise { 
            c := ~c
        }
    }
    
    for (i <- 0 until G) {
        d(i)(0) := io.d.payload.fragment.d(i).re
        d(i)(1) := RegNextWhen(io.d.payload.fragment.d(i).im,io.d.fire)
        when(~c) {
            q(i)(0) := d(i)(0)
            q(i)(1) := d(i)(1)
        }.otherwise { 
            q(i)(1) := d(i)(0)
            q(i)(0) := d(i)(1)
        }
    
        io.q.payload.fragment(i)(0) := RegNextWhen(q(i)(0),io.d.fire)
        io.q.payload.fragment(i)(1) := q(i)(1)
    }

    io.q.payload.last := misc.delay(delay,2).delayPulse(io.d.payload.last,io.d.fire)

    def delay = 1
}

class RI2C(G:Int, dW:Int) extends Area {
    val io = new Area {
        val d = StreamedBus(Vec(Vec(FixReal(dW),2),G))
        val q = StreamedBus((VecComplex(G,dW)))
    }
    io.d.ready := io.q.ready
    io.q.valid   := io.d.fire
    val d = Vec(Vec(FixReal(dW),2),G)
    val q = Vec(Vec(FixReal(dW),2),G)
    val c = RegInit(False)
    when(io.d.fire) {
        when(io.d.payload.last) {
            c := False
        }.otherwise { 
            c := ~c
        }
    }
    for(i <- 0 until G) {
        d(i)(0) := io.d.payload.fragment(i)(0)
        d(i)(1) := RegNextWhen(io.d.payload.fragment(i)(1),io.d.fire)
        when(~c) {
            q(i)(0) := d(i)(0)
            q(i)(1) := d(i)(1)
        }.otherwise { 
            q(i)(1) := d(i)(0)
            q(i)(0) := d(i)(1)
        }
        io.q.payload.fragment.d(i).re := RegNextWhen(q(i)(0),io.d.fire)
        io.q.payload.fragment.d(i).im := q(i)(1)
    }


    io.q.payload.last := misc.delay(delay,2).delayPulse(io.d.payload.last,io.d.fire)

    def delay = 1
}


