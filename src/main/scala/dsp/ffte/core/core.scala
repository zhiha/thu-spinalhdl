package ffte.core

import spinal.core._
import spinal.lib._
import ffte.property.{getFFTIW,getFFTOW}
import ffte.algorithm.FFTGen.{combineFT,Method}
import ffte.types.{VecComplex}
import ffte.default.config

trait fft {
    def dW: Int
    def oW: Int
    def delay:Int
}

trait combine[T<:Method] {
    def S : Int
    def p : combineFT[T]
    val dW     = getFFTIW()
    val oW     = getFFTOW()
    val first  = p.ib
    val second = p.ob
    val N      = p.N
    val first_shift  = Math.round(S*Math.log(first.N)/Math.log(second.N*first.N)).toInt
    val second_shift = S - first_shift
    val interW       = Math.round((oW*Math.log(first.N)+dW*Math.log(second.N))/Math.log(second.N*first.N)).toInt
}

trait widthed {
    val dW=getFFTIW()
    val oW=getFFTOW()
}

trait samewidthed {
    def dW:Int
    def oW = dW
}

object StreamedBus {
    def apply[T <: Data](itype:HardType[T]) = Stream(Fragment(itype()))
}

case class streamedIO(G:Int,dW:Int,oW:Int) extends Bundle {
    val d =  slave(StreamedBus(VecComplex(G,dW)))
    val q = master(StreamedBus(VecComplex(G,oW)))
}

case class StreamedGroupConfig(groups:Int) {
    def on[B](body : => B) = {
        StreamedGroupProperty(this) on {
            body
        }
    }
    def apply[B](body : => B): B = on(body)
    def setAsDefault() = StreamedGroupProperty.setDefault(this)
}

object StreamedGroupProperty extends ScopeProperty[StreamedGroupConfig] {
    var _default: StreamedGroupConfig = config.DefaultStreamedGroupConfig
}

object getStreamedGroup { 
    def apply(): Int = StreamedGroupProperty.get.groups
}