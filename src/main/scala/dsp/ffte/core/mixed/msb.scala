package ffte.core.mixed

import spinal.core._
import spinal.lib._

import ffte.algorithm.FFTGen.{msbFFT}

class msb[T<:Mixed](val S:Int,val p:msbFFT[T]) extends mixedComponents[T] {

    val r = new rotator(interW,p.p.first.store_tab,p.p.second.load_tab,p.p.rgen)

    f_block.u.io.q   >> r.io.d
    r.io.q           >> s_block.u.io.d
    
    io.d >> f_block.u.io.d
    io.q << s_block.u.io.q
    
    def delay = f_block.delay + s_block.delay + r.delay
}
