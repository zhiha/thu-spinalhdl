package ffte.core.mixed

import spinal.core._

import ffte.property.{FFTIOConfig}
import ffte.core.{combine}
import ffte.core.flat.Flat
import ffte.core.streamed.{Streamed,streamedComponents}
import ffte.algorithm.FFTGen.{Method}

trait mixedComponents[T<:Method] extends streamedComponents with combine[T] {
    val rgen = p.p.rgen
    val io = iogen
    val f_block = FFTIOConfig(dW,interW) on new Area {
        val u = if(rgen) {
            val m = new Streamed(first_shift)
            m(first,"f")
        } else {
            val m = new Flat(first_shift)
            new m.toStreamed(first,"s").asInstanceOf[streamedComponents] 
        }
        val delay = u.delay
    }
    val s_block = FFTIOConfig(interW,oW) on new Area {
        val u = if(rgen) {
            val m = new Flat(second_shift)
            new m.toStreamed(second,"s").asInstanceOf[streamedComponents] 
        } else {
            val m = new Streamed(second_shift)
            m(second,"s")
        } 
        val delay = u.delay
    }
}