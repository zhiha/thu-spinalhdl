package ffte.core.mixed

import spinal.core._
import spinal.lib._

import ffte.types.{VecComplex,FixComplex}
import ffte.core.samewidthed
import ffte.property.{getTwiddleResolution}
import ffte.core.streamed.{streamedComponents}

class rotator(val dW:Int,first:Array[Int],second:Array[Int],rgen:Boolean) extends streamedComponents with samewidthed {
    val io = iogen
    io.d.ready := io.q.ready
    val fire = io.d.fire
    io.q.valid := fire
    val tW = 1-getTwiddleResolution()
    val tr = getTwiddleResolution()
    val N  = if(rgen) first.size else second.size
    val rN = first.size * second.size
    val d  = RegNextWhen(io.d.payload.fragment,fire)
    val cnt = Counter(N,inc=fire)
    def ttab = for(n <- 0 until N) yield {
        val r = VecComplex(G,tW,tr)
        for(g <- 0 until G) {
            r.d(g) := (if(rgen) FixComplex(tW,tr).expj(-second(g)*first(n),rN) else FixComplex(tW,tr).expj(-second(n)*first(g),rN))
        }
        r
    }
    val mem = Mem(VecComplex(G,tW,tr),initialContent = ttab)
    val f = mem.readSync(
        address = cnt,
        enable  = fire
    )
    val q = RegInit(VecComplex(G,dW+tW,d.resolution+tr).zero)
    when(fire) {
        when(io.d.payload.last) {
            cnt.clear
        }
        q := d*f
    }
    io.q.payload.fragment := q.cut(dW,-tr-1,d.resolution)
    def delay = 2
    io.q.payload.last := Delay(io.d.payload.last,delay,fire)
}