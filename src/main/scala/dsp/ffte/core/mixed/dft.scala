package ffte.core.mixed
import spinal.core._
import spinal.lib._

import ffte.core.streamed.{streamedComponents}
import ffte.core.mixed.{Mixed}
import ffte.core.{widthed,StreamedGroupConfig}
import ffte.property.{getShiftMethod}
object MFFT extends widthed {
    def apply(G:Int,N:Int) = {
        val s = getShiftMethod(N*G) + (if(dW>oW) (dW-oW) else 0)
        new dft(G,N,s,dW,oW)
    }
    def apply(G:Int,N:Int,S:Int) = new dft(G,N,S,dW,oW)
    def apply(G:Int,N:Int,S:Int,_dW:Int) = new dft(G,N,S,_dW,_dW)
    def apply(G:Int,N:Int,S:Int,_dW:Int,_oW:Int) = new dft(G,N,S,_dW,_oW)
}

object MIFFT extends widthed {
    def apply(G:Int,N:Int) = {
        val s = getShiftMethod(N*G) + (if(dW>oW) (dW-oW) else 0)
        new dft(G,N,s,dW,oW,true)
    }
    def apply(G:Int,N:Int,S:Int) = new dft(G,N,S,dW,oW,true)
    def apply(G:Int,N:Int,S:Int,_dW:Int) = new dft(G,N,S,_dW,_dW,true)
    def apply(G:Int,N:Int,S:Int,_dW:Int,_oW:Int) = new dft(G,N,S,_dW,_oW,true)
}

class dft(g:Int,n:Int,val S:Int,val dW:Int,val oW:Int,inv:Boolean=false) extends streamedComponents {
    override val G = g
    val in_tab  = Array.ofDim[Int](n,g)
    val out_tab = Array.ofDim[Int](n,g)
    val io = iogen
    val gArea = StreamedGroupConfig(g) on new Area {
        val gp = Mixed.gen(n*g)
        // println(s"in dft $n,$g,$gp")
        val m  = new Mixed(S)
        val u  = m(gp.fft,"top")
        u.io.d <> (if(inv) streamedComponents.inv(io.d) else io.d)
        u.io.q <> (if(inv) streamedComponents.inv(io.q) else io.q)
        for (i <- 0 until n;k <- 0 until g) {
            in_tab(i)(k) = gp.load_tab(i*g+k)
            out_tab(i)(k) = gp.store_tab(i+k*n)
        }
    }
    def delay = gArea.u.delay
}

class rdft(g:Int,n:Int,val S:Int,val dW:Int,val oW:Int,inv:Boolean=false) extends streamedComponents {
    override val G = g
    val in_tab  = Array.ofDim[Int](n,g)
    val out_tab = Array.ofDim[Int](n,g)
    val io = iogen
    val gArea = StreamedGroupConfig(g) on new Area {
        val gp = Mixed.rgen(n*g)
        val m  = new Mixed(S)
        val u  = m(gp.fft,"top")
        u.io.d <> (if(inv) streamedComponents.inv(io.d) else io.d)
        u.io.q <> (if(inv) streamedComponents.inv(io.q) else io.q)
        for (i <- 0 until n;k <- 0 until g) {
            in_tab(i)(k) = gp.load_tab(i+k*n)
            out_tab(i)(k) = gp.store_tab(i*g+k)
        }
    }
    def delay = gArea.u.delay
}