package ffte.core.mixed
import ffte.algorithm.FFTGen.{ffter,Method,genericFT,psbFFT,msbFFT}
import ffte.algorithm.{FFTGen,Prime}
import ffte.core.streamed.{Streamed,streamedComponents,sdft2}
import ffte.core.flat.Flat
import ffte.core.{getStreamedGroup}

object Mixed {
    def choose_factor(N:Int) = {
        val G = getStreamedGroup()
        assert(N%G==0,s"$G%$N must be 0, in mix gen")
        (G,N/G)        
    }
    def gen(N:Int) : ffter[Method] = {
        val (i,o) = choose_factor(N)
        val ib = FFTGen.gen[Flat](i).asInstanceOf[ffter[Method]]
        val ob = FFTGen.gen[Streamed](o).asInstanceOf[ffter[Method]]
        if(Prime.gcd(i,o)==1) FFTGen.psb(ib,ob,false) 
        else FFTGen.msb(ib,ob,false)
    }
    def rgen(N:Int) : ffter[Method] = {
        val (o,i) = choose_factor(N)
        val ib = FFTGen.rgen[Streamed](i).asInstanceOf[ffter[Method]]
        val ob = FFTGen.rgen[Flat](o).asInstanceOf[ffter[Method]]
        if(Prime.gcd(i,o)==1) FFTGen.psb(ib,ob,true) 
        else FFTGen.msb(ib,ob,true)
    }
}

class Mixed(shift:Int) extends Method {
    def apply(entity: genericFT, prefix: String): streamedComponents = {
        entity match {
            case d : psbFFT[Mixed]    => new psb(shift,d)
            case d : msbFFT[Mixed]    => new msb(shift,d)
            case _                    => new sdft2(shift)
        }
    }
}