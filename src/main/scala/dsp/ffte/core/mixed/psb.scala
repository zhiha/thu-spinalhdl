package ffte.core.mixed

import spinal.core._
import spinal.lib._

import ffte.algorithm.FFTGen.{psbFFT}

class psb[T<:Mixed](val S:Int,val p:psbFFT[T]) extends mixedComponents[T] {

    f_block.u.io.q   >> s_block.u.io.d
    
    io.d >> f_block.u.io.d
    io.q << s_block.u.io.q
    
    def delay = f_block.delay + s_block.delay
}
