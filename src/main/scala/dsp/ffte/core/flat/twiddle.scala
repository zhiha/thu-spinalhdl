package ffte.core.flat

import spinal.core._
import breeze.math.Complex
import ffte.types.{FixReal,FixComplex}
import ffte.algorithm.Prime
import ffte.property.{getTwiddleResolution}
trait twiddle {
    def simple = this match {
        case w:TW => w._simple
        case _ => this
    }
    def needMul = this.simple match {
        case w:TW => true
        case _    => false
    }
    def mulTimes = this.simple match {
        case TW(_,n) => if(n==8) 2 else 4
        case _ => 0
    }
    def width = 1-getTwiddleResolution()
    def mul(d:FixComplex) = this.simple match {
        case T1  => d
        case T_1 => -d
        case TJ  => d.j
        case T_J => -d.j
        case TW(k,n) => {
            val r = 1<<(width-1)
            if(n==8) {
                val cospi_4 = Math.cos(Math.PI/4)
                val f = FixReal(width).fromInt(Math.round(cospi_4*r).toInt) 
                val c = FixComplex(d.width+1)
                val e = FixComplex(d.width)
                c.re := d.re.expand - d.im
                c.im := d.re.expand + d.im
                e := (c*f).cut(d.width,width-1,d.resolution)
                k match {
                    case 1 => e
                    case 3 => e.j
                    case 5 => -e
                    case 7 => -e.j
                    case _ => FixComplex(d.width).zero
                }
            } else {
                val c = FixComplex(width)
                val cos = Math.cos(2*k*Math.PI/n)
                val sin = Math.sin(2*k*Math.PI/n)
                
                c.re.d.raw := S(Math.round(cos*r).toInt,width bits)
                c.im.d.raw := S(Math.round(sin*r).toInt,width bits)
                (d*c).cut(d.width,width-1,d.resolution)
            }
        }
    }
    def Rotate(d:FixComplex,ce:Bool) = {
        val ra = new Area {
            val r = RegInit(FixComplex(d.width).zero)
            when(ce) {
                r := mul(d)
            }
        }
        ra.r
    }
}

case object T_1 extends twiddle 
case object T1  extends twiddle 
case object TJ  extends twiddle 
case object T_J extends twiddle 

case class TW(k :Int,n : Int) extends twiddle {
    def _simple = {
        val vk = (k+2*n)%n
        vk match {
            case 0                  => T1
            case vk if(vk*2 == n)   => T_1
            case vk if(vk*4 == n)   => TJ
            case vk if(vk*4 == 3*n) => T_J
            case _ => { 
                val g = Prime.gcd(vk,n) 
                TW(vk/g,n/g)
            }
         }
    }
}


