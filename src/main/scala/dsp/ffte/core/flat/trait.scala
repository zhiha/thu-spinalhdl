package ffte.core.flat

import spinal.core._
import spinal.lib._

import ffte.types.VecComplex
import ffte.property.{getFFTIW,getFFTOW,FFTIOConfig}
import ffte.algorithm.FFTGen.{Method,combineFT}
import ffte.core.{fft,combine}
case class flatIO(N:Int,dW:Int,oW:Int) extends Bundle  with IMasterSlave {
    val d  = VecComplex(N,dW)
    val q  = VecComplex(N,oW)
    val ce = in(Bool)
    def asMaster = {
        out(q)
        in(d)
    }
}

trait flatComponents extends Component with fft {
    def N  : Int
    val io : flatIO
    def iogen = master(flatIO(N,dW,oW))
}

trait combineFlatComponents[T<:Method] extends flatComponents with combine[T] {
    val fg = new Flat(first_shift)
    val sg = new Flat(second_shift)
}
