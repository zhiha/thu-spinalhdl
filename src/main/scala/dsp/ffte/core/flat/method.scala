package ffte.core.flat

import ffte.algorithm.FFTGen.{Method,genericFT,DFT,psbFFT,msbFFT,wpbFFT}
import ffte.core.streamed.streamedComponents
import ffte.misc
import ffte.core.{StreamedGroupConfig,widthed}

import spinal.core._
class Flat(shift:Int) extends Method {
    def apply(entity: genericFT, prefix: String):flatComponents = {
        entity match {
            case d : DFT            => new ndft(d.N,shift)
            case d : psbFFT[Flat]   => new psb(shift,d)
            case d : msbFFT[Flat]   => new msb(shift,d)
            case d : wpbFFT[Flat]   => new wpb(shift,d)
            case _                  => new ndft(2,shift)
        }
    }
    class toStreamed(entity: genericFT, prefix: String) extends streamedComponents with widthed {
        override val G  = entity.N
        val io = iogen
        io.d.ready := io.q.ready
        val fire = io.d.fire
        io.q.valid := fire
        val gArea = StreamedGroupConfig(entity.N) on new Area {
            val u  = apply(entity,prefix)
            u.io.d     := io.d.payload.fragment
            io.q.payload.fragment := u.io.q
            u.io.ce    := fire
        }
        def delay = gArea.u.delay
        io.q.payload.last := misc.delay(delay,G).delayPulse(io.d.payload.last,fire)
    }
}

