package ffte.core.flat

import spinal.core._
import spinal.lib.Delay

import ffte.property.{FFTIOConfig,getTwiddleResolution}
import ffte.algorithm.{circularOrder,FFTMisc}
import ffte.algorithm.FFTGen.{Method,wpbFFT}
import ffte.types.{VecComplex,FixComplex}

class wpb[T<:Method](val S:Int,val p:wpbFFT[T]) extends combineFlatComponents[T] {
    val io = iogen
    val tW = 1-getTwiddleResolution()
    val oc = circularOrder(p.N)
    val increase = Math.round(Math.log(p.N-1)/Math.log(2)-S).toInt
    val (wf,ws) = oc.wfft(tW)
    val o = VecComplex(p.N,oW+increase)
    val f_block = FFTIOConfig(dW,interW) on new Area {
        val u = fg(first,s"f") 
        val delay = u.delay
        u.io.ce := io.ce
    }
    val s_block = FFTIOConfig(interW,oW+increase) on new Area {
        val g = new Flat(second_shift)
        val u = g(second,s"f") 
        val delay = u.delay
        u.io.ce := io.ce
    }
    val fdelay = 1 + f_block.delay + s_block.delay
    val xf = Reg(VecComplex(p.N-1,interW))
    s_block.u.io.d := xf
    for(i <- 1 until p.N) {
        f_block.u.io.d.d(i-1) := io.d.d(i)
        val f = FixComplex(1-getTwiddleResolution(),getTwiddleResolution())
        f.re.fromInt(wf(p.p.first.store_tab(i-1))._1)
        f.im.fromInt(wf(p.p.first.store_tab(i-1))._2)
        when(io.ce) {
            xf.d(i-1) := (f*f_block.u.io.q.d(i-1)).cut(interW,-getTwiddleResolution(),f_block.u.io.q.d(i-1).resolution)
        }
        o.d(i)  := s_block.u.io.q.d(i-1).resize(oW+increase+ws).cut(oW+increase,ws)
    }
    o.d(0) := FixComplex.sumc(Vec((1 until p.N).map(io.d.d(_).resize(oW+increase+S))),p.N-1,0,fdelay,io.ce).cut(oW+increase,S)
    val d0 = Delay(io.d.d(0),fdelay,io.ce).resize(oW+increase+S).cut(oW+increase,S)
    val q = Reg(VecComplex(p.N,oW))
    when(io.ce) {
        for(i <- 0 until p.N) q.d(i) := (d0 + o.d(i)).fixTo(oW,0)
    }
    io.q := q
    def delay = fdelay+1
}
