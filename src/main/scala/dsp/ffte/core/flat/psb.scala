package ffte.core.flat

import spinal.core._
import ffte.property.{FFTIOConfig}
import ffte.algorithm.FFTGen.{Method,psbFFT}
class psb[T<:Method](val S:Int,val p:psbFFT[T]) extends combineFlatComponents[T] {
    val io = iogen
    val f_block = FFTIOConfig(dW,interW) on new Area {
        val u = (0 until second.N).toList.map{ i => fg(first,s"f$i") }
        val delay = u(0).delay
        u.foreach{x => x.io.ce := io.ce}
    }
    val s_block = FFTIOConfig(interW,oW) on new Area {
        val u = (0 until first.N).toList.map{ i => sg(second,s"f$i") }
        val delay = u(0).delay
        u.foreach{x => x.io.ce := io.ce}
    }
    for(i <- 0 until second.N) {
        for(k <- 0 until first.N) {
            s_block.u(k).io.d.d(i) := f_block.u(i).io.q.d(k)
            f_block.u(i).io.d.d(k) := io.d.d(i*first.N+k)
            io.q.d(k*second.N+i)   := s_block.u(k).io.q.d(i)
        }
    }
    def delay = f_block.delay+s_block.delay
}
