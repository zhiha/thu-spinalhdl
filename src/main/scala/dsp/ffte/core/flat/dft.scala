package ffte.core.flat

import spinal.core._
import ffte.types.VecComplex
import ffte.property.{getFFTIW,getFFTOW,getShiftMethod,FFTIOConfig}
import ffte.algorithm.FFTGen
import ffte.core.widthed
import ffte.core.streamed.streamedComponents

class ndft(val N:Int,val S:Int) extends flatComponents {
    assert(N==2,s"simple dft only support 2, but get $N")
    val dW = getFFTIW()
    val oW = getFFTOW()
    val io = iogen
    val u = new dft2(dW,oW+S)
    u.d  := io.d
    io.q := u.q.fixTo(oW,S)
    u.ce := io.ce
    def delay = 1
}

class dft2(dW:Int,oW:Int) extends Area {
    val d = VecComplex(2,dW)
    val q = Reg(VecComplex(2,oW))
    val ce = Bool
    when(ce) {
        if(oW<dW) {
            q.d(0) := (d.d(0)+d.d(1)).fixTo(oW,0)
            q.d(1) := (d.d(0)-d.d(1)).fixTo(oW,0)
        } else {
            q.d(0) := d.d(0)+d.d(1)
            q.d(1) := d.d(0)-d.d(1)
        }
    }
}

object FFFT extends widthed {
    def apply(N:Int) = {
        val s = getShiftMethod(N) + (if(dW>oW) (dW-oW) else 0)
        new dft(N,s,dW,oW)
    }
    def apply(N:Int,S:Int) = new dft(N,S,dW,oW)
    def apply(N:Int,S:Int,_dW:Int) = new dft(N,S,_dW,_dW)
    def apply(N:Int,S:Int,_dW:Int,_oW:Int) = new dft(N,S,_dW,_oW)
}

object FIFFT extends widthed {
    def apply(N:Int) = {
        val s = getShiftMethod(N) + (if(dW>oW) (dW-oW) else 0)
        new dft(N,s,dW,oW,true)
    }
    def apply(N:Int,S:Int) = new dft(N,S,dW,oW,true)
    def apply(N:Int,S:Int,_dW:Int) = new dft(N,S,_dW,_dW,true)
    def apply(N:Int,S:Int,_dW:Int,_oW:Int) = new dft(N,S,_dW,_oW,true)
}

class dft(val N:Int, val S:Int, val dW:Int, val oW:Int, inv:Boolean=false) extends flatComponents {
    val io = iogen
    FFTGen.Winograd
    val gp = FFTGen.rgen[Flat](N)
    val m  = new Flat(S)
    val widthArea = FFTIOConfig(dW,oW) on new Area {
        val default_dW = getFFTIW()
        val default_oW = getFFTOW()
        val u  = m(gp.fft,"top")
        val d  = (if(inv) io.d.swap else io.d)
        val q  = VecComplex(N,oW)
        for(i <- 0 until N) {
            u.io.d.d(i) := d.d(gp.load_tab(i))
            q.d(gp.store_tab(i)) := u.io.q.d(i)
        }
        u.io.ce := io.ce
    }
    io.q        := (if(inv) widthArea.q.swap else widthArea.q)
    def delay = widthArea.u.delay
}

