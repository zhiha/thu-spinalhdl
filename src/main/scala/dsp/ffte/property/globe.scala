package ffte.property

import spinal.core._
import ffte.default.config

case class FFTGlobeConfig(
    fftScaleFactor : Double
){
    def on[B](body : => B) = {
        FFTGlobeProperty(this) on {
            body
        }
    }
    def apply[B](body : => B): B = on(body)
    def setAsDefault() = FFTGlobeProperty.setDefault(this)
}

object FFTGlobeProperty extends ScopeProperty[FFTGlobeConfig] {
    var _default: FFTGlobeConfig = config.DefaultFFTGlobeConfig
}

object getFFTScalaFactor { 
    def apply(): Double = FFTGlobeProperty.get.fftScaleFactor
}
