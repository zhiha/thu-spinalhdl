package ffte.property

import spinal.core._
import ffte.default.config

case class FFTRTLConfig(
      complexMulFast    : Boolean
    , symmetric         : Boolean
    , resolution        : Int
    , twiddleResolution : Int
    , shiftMethod       : (Int) => Int
){
    def on[B](body : => B) = {
        FFTGenRTLProperty(this) on {
            body
        }
    }
    def apply[B](body : => B): B = on(body)
    def setAsDefault() = FFTGenRTLProperty.setDefault(this)
}

object FFTGenRTLProperty extends ScopeProperty[FFTRTLConfig]{
    var _default: FFTRTLConfig = config.DefaultFFTGenRTLConfig
}

object getComplexMulMethod { // true is fast 4 mul complex mul
    def apply(): Boolean = FFTGenRTLProperty.get.complexMulFast
}

object getFixSym{
    def apply(): Boolean = FFTGenRTLProperty.get.symmetric
}

object getResolution {
    def apply(): Int = FFTGenRTLProperty.get.resolution
}

object getTwiddleResolution {
    def apply(): Int = FFTGenRTLProperty.get.twiddleResolution
}

object getShiftMethod {
    def apply(n:Int):Int = FFTGenRTLProperty.get.shiftMethod(n)
}
