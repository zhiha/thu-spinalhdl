package ffte.property

import spinal.core._
import ffte.default.config

case class FFTIOConfig(
      inWidth : Int
    , outWidth : Int
){
    def on[B](body : => B) = {
        FFTIOProperty(this) on {
            body
        }
    }
    def apply[B](body : => B): B = on(body)
    def setAsDefault() = FFTIOProperty.setDefault(this)
}

object FFTIOProperty extends ScopeProperty[FFTIOConfig]{
    var _default: FFTIOConfig = config.DefaultFFTIOConfig
}

object getFFTIW { 
    def apply(): Int = FFTIOProperty.get.inWidth
}

object getFFTOW { 
    def apply(): Int = FFTIOProperty.get.outWidth
}
