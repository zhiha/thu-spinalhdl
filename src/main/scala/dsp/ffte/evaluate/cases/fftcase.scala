package ffte.evaluate.cases
import scala.util.Random._
import scala.collection.mutable.Map
import ffte.algorithm.Prime
import spinal.core._
import ffte.property.getShiftMethod
object FFTCase {
    val some = List(1800,1379,1013,1151,1447,396,89,1723,1021,467,1910,5120,4096,8192,6400)
    def shift(n:Int) = getShiftMethod(n)
    val prime = (for(i <- 1 until 20) yield {
            val n=Prime.primeList(i)
            val s = shift(n)
            (n->s)
    }).toMap
    val basic = prime ++ Map(
            2    -> 1,
            28   -> 3,
            58   -> 4,
            70   -> 4,
            68   -> 4,
            42   -> 3,
            46   -> 4,
            25   -> 3,
            21   -> 3,
            24   -> 3,
            9    -> 2,
            27   -> 3,
            56   -> 4,
            65   -> 4,
            91   -> 4,
            80   -> 4,
            98   -> 4,
            77   -> 4,
            90   -> 4,
            63   -> 4,
            4    -> 1,
            8    -> 2,
            16   -> 2,
            32   -> 3,
            64   -> 3
        )
    val cases = basic ++ Map(
            128  -> 4,
            256  -> 4,
            512  -> 5,
            1024 -> 5,
            2048 -> 6,
            1440 -> 6,
            1600 -> 6,
            1920 -> 6,
            800  -> 5,
            640  -> 5,
            720  -> 5,
            121  -> 4,
            105  -> 4
        ) ++  (for(n <- some) yield {
            val s = (log2Up(n)+1)/2
            (n->s)
        })
    def extend =  cases ++  (for(i <- 21 until 310) yield {
            val n=Prime.primeList.reverse(i)
            val s = shift(n)
            (n->s)
        }) ++    
        (for(i<- 0 until 290) yield {
            val n = Math.round(nextDouble()*2048).toInt+2
            val k = Prime.repesent(n).keySet.max
            val s = shift(n)
            (n->s)
        })
    val full = (for(n <- 2 until 2050) yield {
        val s = shift(n)
        (n->s)
    }).toMap ++ (for(i <- 12 until 17) yield{
        val n = 1<<i
        val s = shift(n)
        (n->s)
    }) 
}