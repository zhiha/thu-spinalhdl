package ffte.evaluate.cases

import java.io.FileWriter
import java.text.SimpleDateFormat
import java.util.Date

import ffte.default.core
import ffte.property.getFFTIW
trait Sim {
    var N       = 2
    var S       = 0
    var K       = 0
    var CC      = 30
    var C       = 1000
    var ok      = true
    var ook     = true
    var inv     = false
    var debug   = 0
    var idx     = 0
    var rgen    = false
    var failcnt = 0
        
    def next : Unit = { idx += 1}
    def test_vectors = {
        val v = core.GlobeSetting.testvector(N,S,inv)
        List.fill(CC)(v.test_vectors(K,getFFTIW()))
    }
    def done(initial: =>Unit,body: =>Unit) : Boolean = {
        initial
        while(idx<C) {
            ook = true
            body
            if(!ook) {
                println(s"failure at $idx")
            }
        }
        ok
    }
}

object Sim {
    def log2file(file:String,msg:String) : Unit = {
        val now: Date = new Date()
        val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val date = dateFormat.format(now)
        val out = new FileWriter(file,true)
        val vs = date + " done:" + msg 
        out.write(vs+"\n")
        out.close()
    }
}