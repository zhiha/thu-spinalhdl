package ffte.evaluate.vectors

import breeze.math.Complex
import scala.util.Random._
import ffte.algorithm.FFTMisc

import ffte.native.{NFFT,NIFFT,Native}
import ffte.algorithm.FFTGen

case class NativeVector(N:Int,S:Int,inv:Boolean=false) extends genVector {
    FFTGen.Winograd
    val fp = FFTGen.gen[Native](N)
    val t2f = new NFFT(fp,S)
    val f2t = new NIFFT(fp,S)
    def TV(sx:Int) = {
        val x = new Array[Complex](N)
        for (i <- 0 until N) { 
            x(i) = Complex(nextDouble()-0.5,nextDouble()-0.5)*(1<<sx)
        }
        val y = if(inv) f2t.transform(x) else t2f.transform(x)
        TestVectors(TestVectors.fromVC(x,0),TestVectors.fromVC(y,0))
    } 
    def NV(k:Int,s:Int) = {
		val y = new Array[Complex](N)
		for (i <- 0 until N) { 
			y(i) = Complex(0.0,0.0)
		}
		y(k) = Complex(1.0/128.0,0.0)*(1<<(S+s))
		val x = if(inv) t2f.transform(y) else f2t.transform(y)
		TestVectors(TestVectors.fromVC(x,0),TestVectors.fromVC(y,0))
	}
    def IV(k:Int,s:Int) = {
		val y = new Array[Complex](N)
		for (i <- 0 until N) { 
			y(i) = Complex(0.0,0.0)
		}
		y(k) = Complex(0.0,1.0/128.0)*(1<<(S+s))
		val x = if(inv) t2f.transform(y) else f2t.transform(y)
		TestVectors(TestVectors.fromVC(x,0),TestVectors.fromVC(y,0))
	}
}
