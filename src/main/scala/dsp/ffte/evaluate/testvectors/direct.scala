package ffte.evaluate.vectors

import scala.math._
import breeze.math._
import scala.util.Random._
import ffte.algorithm.FFTMisc

case class DirectVector(N:Int,S:Int,inv:Boolean=false) extends genVector {

    def TV(sx:Int) = {
        val x = new Array[Complex](N)
        for (i <- 0 until N) { 
            x(i) = Complex(nextDouble()-0.5,nextDouble()-0.5)
        }
        val y = if(inv) FFTMisc.idft(x) else FFTMisc.dft(x)
        TestVectors(TestVectors.fromVC(x,sx),TestVectors.fromVC(y,sx-S))
    }

	def NV(k:Int,s:Int) = {
		val y = new Array[Complex](N)
		for (i <- 0 until N) { 
			y(i) = Complex(0.0,0.0)
		}
		y(k) = Complex(1.0/128.0,0.0)
		val x = if(inv) FFTMisc.dft(y) else FFTMisc.idft(y)

		TestVectors(TestVectors.fromVC(x,s),TestVectors.fromVC(y,s-S))
	}

	def IV(k:Int,s:Int) = {
		val y = new Array[Complex](N)
		for (i <- 0 until N) { 
			y(i) = Complex(0.0,0.0)
		}
		y(k) = Complex(0.0,1.0/128.0)
		val x = if(inv) FFTMisc.dft(y) else FFTMisc.idft(y)

		TestVectors(TestVectors.fromVC(x,s),TestVectors.fromVC(y,s-S))
	}
}
