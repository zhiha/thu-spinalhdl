package ffte.evaluate.vectors

import breeze.math.Complex

object Cplx {
    implicit def cplxi2Complex(x : Cplx) : Complex = Complex(x.re,x.im)
    def iabs(a:Int) = if(a<0) -a else a
}

case class Cplx(re:Int,im:Int) {
    def -(that:Cplx) = Cplx(this.re-that.re,this.im-that.im)
    def norm1 : Int= Cplx.iabs(re)+Cplx.iabs(im)
}
case class TestVectors(d:Array[Cplx],q:Array[Cplx])    

trait genVector {
    val N : Int
    val S : Int
    def TV(sx:Int) : TestVectors
    def NV(k:Int,s:Int)  : TestVectors
    def IV(k:Int,s:Int)  : TestVectors
    var inc = 0
    def test_vectors(K:Int,dW:Int) = K match {
        case 0              => TV(dW-3)
        case k if(k == -N)  => { inc += 1; IV(inc%N,dW-3)}
        case N              => NV(0,dW-3)
        case k if(k<0)      => IV((-k)%N,dW-3)
        case _              => NV(K%N,dW-3)
    }
}

object TestVectors {
    def d2i(x: Double, s :Int) = {
        Math.round(x*Math.pow(2,s)).toInt
    }
    def c2ci(x:Complex,s:Int) = Cplx(d2i(x.re,s),d2i(x.im,s))
    def fromVC(x: Array[Complex], s:Int) = x.map(c2ci(_,s))
}
    
