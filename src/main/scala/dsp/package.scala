
import spinal.core.sim._
import spinal.sim._
import spinal.core._
import scala.math.max

import dsp.basic._

package object dsp {
  implicit class SimSFixPimper(sf:SFix){
    import sf._

    def #=(value: BigDecimal): Unit = {
      assert(value <= maxValue, s"Literal $value is too big to be assigned in $this")
      assert(value >= minValue, s"Literal $value is too small to be assigned in this $this")

      val shift = -minExp
      val ret = if (shift >= 0) // ret this is the "binary string" of value at specific precision
        (value * BigDecimal(BigInt(1) << shift)).toBigInt
      else
        (value / BigDecimal(BigInt(1) << -shift)).toBigInt
      setLong(raw, ret.toLong)
    }

    def #=(value: Double): Unit = #=(BigDecimal(value))

    def toDouble = raw.toBigInt.toDouble / (1 << -minExp)

    def reciprocal:SFix={
      val ret = Reg(SFix(sf.maxExp exp,sf.minExp exp)) init(0.0)
      val cnt = Reg(UInt(6 bits)) init(U(0))
      val cordic = new Cordic(sf.maxExp,-sf.minExp,Config(VECTOR,LINEAR))
      cordic.io.input.x := sf.truncated
      cordic.io.input.y := 1.0
      cordic.io.input.z := 0.0
      when(cnt === U(19)){ 
        ret := cordic.io.output.z.truncated
        cnt := 0 
      }.otherwise{
        cnt := cnt + 1
      } 
      ret
    }

    def abs :SFix ={
      val ret = SFix(sf.maxExp exp,sf.minExp exp)
      ret.raw := Mux(sf.raw.msb,~sf.raw+S(1),sf.raw)
      ret
    }
    
  }

  implicit class SimDoublePimper(d:Double){
  
    def /(sf:SFix) :SFix = {
      val ret = Reg(SFix(sf.maxExp exp,sf.minExp exp)) init(0.0)
      val cnt = Reg(UInt(6 bits)) init(U(0))
      val cordic = new Cordic(sf.maxExp,-sf.minExp,Config(VECTOR,LINEAR))
      cordic.io.input.x := sf.truncated
      cordic.io.input.y := 1.0
      cordic.io.input.z := 0.0
      when(cnt === U(19)){ 
        ret := cordic.io.output.z.truncated
        cnt := 0 
      }.otherwise{
        cnt := cnt + 1
      } 
      ret
    }
    
  }
  
}
