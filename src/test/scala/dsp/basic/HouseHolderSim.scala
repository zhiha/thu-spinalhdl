import dsp._
import dsp.basic._
import spinal.core._
import spinal.sim._
import spinal.core.sim._
import org.scalatest.funsuite._

class HouseHolderSim extends AnyFunSuite{
    test("HouseHolder test"){
        SimConfig.withWave.doSim(new HouseHolder(8,20,3)){dut =>
            var cnt = 0
            dut.clockDomain.forkStimulus(10)
            dut.clockDomain.withSyncReset()
            dut.io.data.vec(0) #= 0.0
            dut.io.data.vec(1) #= 0.0
            dut.io.data.vec(2) #= 0.0
            dut.io.valid #= false
            for(idx <- 0 to 800){
                dut.clockDomain.waitSampling()
                if(dut.io.ready.toBoolean){     
                    if(cnt == 0){
                        dut.io.data.vec(0) #= 1.0
                        dut.io.data.vec(1) #= 3.0
                        dut.io.data.vec(2) #= 2.0
                        dut.io.valid #= true
                    }else if(cnt == 1){
                        dut.io.data.vec(0) #= 2.0
                        dut.io.data.vec(1) #= 0.0
                        dut.io.data.vec(2) #= 0.0
                        dut.io.valid #= true
                    }else if(cnt == 2){
                        dut.io.data.vec(0) #= 0.0
                        dut.io.data.vec(1) #= 2.0
                        dut.io.data.vec(2) #= 0.0
                        dut.io.valid #= true
                    }else if(cnt == 3){
                        dut.io.data.vec(0) #= 0.0
                        dut.io.data.vec(1) #= 0.0
                        dut.io.data.vec(2) #= 2.0
                        dut.io.valid #= true
                    }
                    cnt = cnt + 1
                }
                if(dut.io.outvalid.toBoolean){
                    println(idx)
                    print(dut.io.outputH)  
                }                  
                
            }
        }
    }
    test("HouseHolder test1"){
        SimConfig.withWave.doSim(new HouseHolder(8,20,2)){dut =>
            var cnt = 0
            dut.clockDomain.forkStimulus(10)
            dut.clockDomain.withSyncReset()
            dut.io.data.vec(0) #= 0.0
            dut.io.data.vec(1) #= 0.0
            dut.io.valid #= true
            for(idx <- 0 to 420){
                dut.clockDomain.waitRisingEdge()
                if(dut.io.ready.toBoolean){ 
                    println(idx)
                    print(dut.io.outputH)      
                    if(cnt == 0){
                        dut.io.data.vec(0) #= 4.0
                        dut.io.data.vec(1) #= 3.0
                    }else if(cnt == 1){
                        dut.io.data.vec(0) #= 3.0
                        dut.io.data.vec(1) #= 4.0
                    }else if(cnt == 2){
                        dut.io.data.vec(0) #= 4.0
                        dut.io.data.vec(1) #= 3.0
                    }else if(cnt == 3){
                        dut.io.data.vec(0) #= 3.0
                        dut.io.data.vec(1) #= 4.0
                    }
                    cnt = cnt + 1
                }                  
                
            }
        }
    }
}