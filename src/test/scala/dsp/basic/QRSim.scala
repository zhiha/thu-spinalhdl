import dsp._
import dsp.basic._
import spinal.core._
import spinal.sim._
import spinal.core.sim._
import org.scalatest.funsuite._

class QRSim extends AnyFunSuite{
    test("test the QR function") {
        SimConfig.withWave.doSim(new QR(8,20,3)){dut =>
            dut.clockDomain.forkStimulus(10)
            dut.clockDomain.withAsyncReset()
            dut.io.valid #= false
            var cnt = 0
            for(idx <- 0 to 800){
                dut.clockDomain.waitRisingEdge()
                if(dut.io.ready.toBoolean){
                    if(cnt==0){
                        dut.io.input.mat(0).vec(0) #= 0.0
                        dut.io.input.mat(0).vec(1) #= 0.0
                        dut.io.input.mat(0).vec(2) #= 2.0
                        dut.io.input.mat(1).vec(0) #= 3.0
                        dut.io.input.mat(1).vec(1) #= 4.0
                        dut.io.input.mat(1).vec(2) #= 1.0
                        dut.io.input.mat(2).vec(0) #= 1.0
                        dut.io.input.mat(2).vec(1) #= -2.0
                        dut.io.input.mat(2).vec(2) #= 1.0
                        dut.io.valid #= true
                    }
                    if(cnt==1){
                        dut.io.input.mat(0).vec(0) #= 1.0
                        dut.io.input.mat(0).vec(1) #= 3.0
                        dut.io.input.mat(0).vec(2) #= 2.0
                        dut.io.input.mat(1).vec(0) #= 2.0
                        dut.io.input.mat(1).vec(1) #= 2.0
                        dut.io.input.mat(1).vec(2) #= 3.0
                        dut.io.input.mat(2).vec(0) #= 3.0
                        dut.io.input.mat(2).vec(1) #= 1.0
                        dut.io.input.mat(2).vec(2) #= 1.0
                        dut.io.valid #= true
                    }
                    if(cnt==2){
                        dut.io.input.mat(0).vec(0) #= 0.0
                        dut.io.input.mat(0).vec(1) #= 0.0
                        dut.io.input.mat(0).vec(2) #= 2.0
                        dut.io.input.mat(1).vec(0) #= 3.0
                        dut.io.input.mat(1).vec(1) #= 4.0
                        dut.io.input.mat(1).vec(2) #= 1.0
                        dut.io.input.mat(2).vec(0) #= 1.0
                        dut.io.input.mat(2).vec(1) #= -2.0
                        dut.io.input.mat(2).vec(2) #= 1.0
                        dut.io.valid #= true
                    }
                    cnt = cnt + 1
                }else{
                    //dut.io.valid #= false
                }
                if(dut.io.outvalid.toBoolean){
                    println("*****************R***********************")
                    print(dut.io.outputR)
                    println("*****************Q***********************")
                    print(dut.io.outputQ)
                }
                
            }
        }
    }
}