import dsp._
import dsp.basic._
import spinal.core._
import spinal.sim._
import spinal.core.sim._
import scala.util.Random
import scala.math.{Pi}
import breeze.numerics.{cos,sin,sqrt,atan,abs}
import scala.collection.mutable.Queue
import org.scalatest.funsuite._
case class CordicSimData(x:Double,y:Double,z:Double)

class CordicSim extends AnyFunSuite{
    test("test the funciton of divide"){
        SimConfig.withWave.doSim(new Cordic(3,16,Config(VECTOR,LINEAR))){dut =>
            dut.clockDomain.forkStimulus(period = 10)
            val rand = new Random(20)
            var startCount = 0
            var readyRecieve = false
            var failcnt = 0
            val simData:Queue[CordicSimData]=Queue[CordicSimData]()
            val resultData:Queue[CordicSimData]=Queue[CordicSimData]()
            val refData:Queue[CordicSimData]=Queue[CordicSimData]()
            println("[SIM START] ")
            fork {
                while(true){
                dut.clockDomain.waitRisingEdge()
                if(readyRecieve==true){
                    val x = dut.io.output.x.toDouble
                    val y = dut.io.output.y.toDouble
                    val z = dut.io.output.z.toDouble
                    resultData.enqueue(CordicSimData(x,y,z))
                    }
                }
            }
            fork {
                while(true){
                    dut.clockDomain.waitRisingEdge()
                    if(!resultData.isEmpty && !refData.isEmpty && readyRecieve){
                        val result = resultData.dequeue()
                        val ref = refData.dequeue()
                        val sim = simData.dequeue()
                        if(abs(result.z-ref.z)<0.001){
                            println(Console.GREEN)
                            println(s"[SIMDATA] ${sim.x} ${sim.y}")
                            println(s"[REFDATA] ${ref.z}")
                            println(s"[RESULTDATA] ${result.z} passed")
                        }
                        else{
                            failcnt += 1
                            println(Console.RED)
                            println(s"[SIMDATA] ${sim.x} ${sim.y}")
                            println(s"[REFDATA] ${ref.z}")
                            println(s"[RESULTDATA] ${result.z} failed")
                        }
                    }
                }
            }
            for (idx <- 0 to 10000){
                dut.clockDomain.waitRisingEdge()
                if(startCount==19) readyRecieve = true else startCount += 1
                val x = rand.nextDouble()*2+1
                val y = 1.0
                val z = 0.0
                refData.enqueue(CordicSimData(0.0,0.0,(y/x)))
                simData.enqueue(CordicSimData(x,y,z))
                dut.io.input.x #=  x
                dut.io.input.y #=  y
                dut.io.input.z #=  0.0
            }
            println(s"TEST FAIL NUMBER RATIO IS ${failcnt} / ${10000}")
        }
    }
    test("test the funciton of amplitude and phase"){
        SimConfig.withWave.doSim(new Cordic(2,16,Config(VECTOR,CIRCULAR))){dut =>
            dut.clockDomain.forkStimulus(period = 10)
            val rand = new Random(20)
            var startCount = 0
            var readyRecieve = false
            var failcnt = 0
            val simData:Queue[CordicSimData]=Queue[CordicSimData]()
            val resultData:Queue[CordicSimData]=Queue[CordicSimData]()
            val refData:Queue[CordicSimData]=Queue[CordicSimData]()
            println("[SIM START] ")
            fork {
                while(true){
                dut.clockDomain.waitRisingEdge()
                if(readyRecieve==true){
                    val x = dut.io.output.x.toDouble
                    val y = dut.io.output.y.toDouble
                    val z = dut.io.output.z.toDouble
                    resultData.enqueue(CordicSimData(x,y,z))
                    }
                }
            }
            fork {
                while(true){
                    dut.clockDomain.waitRisingEdge()
                    if(!resultData.isEmpty && !refData.isEmpty && readyRecieve){
                        val result = resultData.dequeue()
                        val ref = refData.dequeue()
                        val sim = simData.dequeue()
                        if(abs(result.x-ref.x)<0.001 && abs(result.z-ref.z)<0.001){
                            println(Console.GREEN)
                            println(s"[SIMDATA] ${sim.x} ${sim.y}")
                            println(s"[REFDATA] ${ref.x} ${ref.z}")
                            println(s"[RESULTDATA] ${result.x} ${result.z} passed")
                        }
                        else{
                            failcnt += 1
                            println(Console.RED)
                            println(s"[SIMDATA] ${sim.x} ${sim.y}")
                            println(s"[REFDATA] ${ref.x} ${ref.z}")
                            println(s"[RESULTDATA] ${result.x} ${result.z} failed")
                        }
                    }
                }
            }
            for (idx <- 0 to 10000){
                dut.clockDomain.waitRisingEdge()
                if(startCount==19) readyRecieve = true else startCount += 1
                val x = rand.nextDouble()*2 - 1
                val y = rand.nextDouble()*2 - 1
                val z = 0.0
                refData.enqueue(CordicSimData(sqrt(x*x+y*y),0.0,if(x<0&&y>0) atan(y/x)+math.Pi else if(x<0&&y<0) atan(y/x)-math.Pi else atan(y/x)))
                simData.enqueue(CordicSimData(x,y,z))
                dut.io.input.x #=  x
                dut.io.input.y #=  y
                dut.io.input.z #=  0.0
            }
            println(s"TEST FAIL NUMBER RATIO IS ${failcnt} / ${10000}")
        }
    }
}